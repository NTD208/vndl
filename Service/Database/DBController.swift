//
//  DBManager.swift
//  PMP
//
//  Created by Thom Vu on 3/11/19.
//  Copyright © 2019 VietLV. All rights reserved.
//

import UIKit
import FMDB

let dbQueue = DispatchQueue.init(label: "dbQueue")
fileprivate let DataBaseName = "gplxa1"
fileprivate let DataBaseType = "db"

class DBController {
    // MARK: - Public
    static var shared = DBController()
    var database: FMDatabase!
    
    init() {
        self.initDatabase()
    }
    
    // MARK: - Practice
    func getAllQuestion() -> [Question] {
        var questions = [Question]()
        openDB()
        guard let resultSet = getResultSet(query: "select * from Question") else {
            closeDB()
            return []
        }
        
        while resultSet.next() {
            let obj = Question(resultSet: resultSet)
            questions.append(obj)
        }
        
        closeDB()
        return questions
    }
    
    func getlistQuestionType() -> [QuestionType] {
        var questionsType = [QuestionType]()
        openDB()
        guard let resultSet = getResultSet(query: "select * from QuestionType") else {
            closeDB()
            return []
        }
        
        while resultSet.next() {
            let obj = QuestionType(resultSet: resultSet)
            questionsType.append(obj)
        }
        
        closeDB()
        return questionsType
    }
    
    func getListQuestion(questionTypeID: Int) -> [Question] {
        var questions = [Question]()
        openDB()
        guard let resultSet = getResultSet(query: "select * from Question where questionType_id = \(questionTypeID)") else {
            closeDB()
            return []
        }
        
        while resultSet.next() {
            let obj = Question(resultSet: resultSet)
            questions.append(obj)
        }
        
        closeDB()
        return questions
    }
    
    func getListQuestion(of test: Test) -> [Question] {
        var question = [Question]()
        let query = "select * from Question inner join TestQuestion on TestQuestion.q_id = Question.id where TestQuestion.t_id = '\(test.id)'"
        openDB()
        guard let resultSet = getResultSet(query: query) else {
            closeDB()
            return []
        }
        
        while resultSet.next() {
            let obj = Question.init(resultSet: resultSet)
            question.append(obj)
        }
        
        closeDB()
        return question
    }
    
    // MARK: - Test
    func getAllTest() -> [Test] {
        var tests = [Test]()
        openDB()
        guard let resultSet = getResultSet(query: "select * from Test") else {
            closeDB()
            return []
        }
        
        while resultSet.next() {
            let test = Test(resultSet: resultSet)
            tests.append(test)
            
            if let historyResultSet = getResultSet(query: "select * from TestResult where testID = \(test.id)"), historyResultSet.next() {
                let obj = TestResult(resultSet: historyResultSet)
                test.testResult = obj
            }
        }
        
        closeDB()
        return tests
    }
    
    func getTestResult(testID: Int) -> TestResult? {
        openDB()
        guard let resultSet = getResultSet(query: "select * from TestResult where testid = '\(testID)'") else {
            closeDB()
            return nil
        }
        
        if resultSet.next() {
            let obj = TestResult(resultSet: resultSet)
            closeDB()
            return obj
        }
        
        closeDB()
        return nil
    }
    
    func getTestResultDetails(testID: Int) -> [TestResultDetail] {
        var testResultDetails = [TestResultDetail]()
        openDB()
        guard let resultSet = getResultSet(query: "select * from TestResultDetail where testid = '\(testID)'") else {
            closeDB()
            return []
        }
        
        while resultSet.next() {
            let obj = TestResultDetail(resultSet: resultSet)
            let query = "select * from Question where id = \(obj.questionID!) limit 1"
            
            if let rs = getResultSet(query: query), rs.next() {
                let question = Question.init(resultSet: rs)
                obj.question = question
            }
            testResultDetails.append(obj)
        }
        
        closeDB()
        return testResultDetails
    }
    
    // MARK: - SignGroup
    func getListSignGroup() -> [SignGroup] {
        var signGroups = [SignGroup]()
        openDB()
        guard let resultSet = getResultSet(query: "select * from SignGroup") else {
            closeDB()
            return []
        }
        
        while resultSet.next() {
            let obj = SignGroup(resultSet: resultSet)

            var signs = [Sign]()
            let query = "select * from Sign where groupID = \(obj.id)"

            if let rs = getResultSet(query: query) {
                while rs.next() {
                    let sign = Sign(resultSet: rs)
                    signs.append(sign)
                }
                obj.signs = signs
            }
            signGroups.append(obj)
        }

        closeDB()
        return signGroups
    }
    
    // MARK: - Sign
    func getListSign(signGroup: SignGroup) -> [Sign] {
        var signs = [Sign]()
        let query = "select * from Sign where groupID = '\(signGroup.id)'"
        openDB()
        guard let resultSet = getResultSet(query: query) else {
            closeDB()
            return []
        }

        while resultSet.next() {
            let obj = Sign(resultSet: resultSet)
            signs.append(obj)
        }

        closeDB()
        return signs
    }
    
    // MARK: - Common
    func excuteUpdate(query: String) -> Bool {
        openDB()
        let success = database!.executeStatements(query)
        closeDB()
        return success
    }
    
    func isExistRecord(query: String) -> Bool {
        openDB()
        let resultSet = try? database.executeQuery(query, values: nil)
        let rs = resultSet?.next() ?? false
        closeDB()
        return rs
    }
    
    func editData() {
        if let path = Bundle.main.path(forResource: "xxx", ofType: "txt") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                let myStrings = data.components(separatedBy: .newlines)
                for i in 0..<myStrings.count {
                    let query = myStrings[i]
                    if !query.isEmpty {
                        let rs = self.excuteUpdate(query: query)
                        let a = 1
                    }
                }
                print("done")
            } catch {
                print(error)
            }
        }
    }
}

// MARK: - Private
private extension DBController {
    
    func truncateDB() {
        
    }
    
    func initDatabase() {
        guard let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
            print("document directory not found")
            return
        }
        
        let dataBaseFilePath = documentPath + "/" + DataBaseName + "." + DataBaseType
        print("databasePath = \(dataBaseFilePath)")
        if FileManager.default.fileExists(atPath: dataBaseFilePath) == false{
            try? FileManager.default.copyfileToUserDocumentDirectory(forResource: DataBaseName, ofType: DataBaseType)
        }
        database = FMDatabase.init(path: dataBaseFilePath)
        openDB()
        database.shouldCacheStatements = true
    }
    
    func clearAllData() {
        
    }
    
    func getResultSet(query: String) -> FMResultSet? {
        return try? database.executeQuery(query, values: nil)
    }
    
    func openDB() {
        database.open()
    }
    
    func closeDB() {
        database.close()
    }
}


extension FileManager {
    func copyfileToUserDocumentDirectory(forResource name: String,
                                         ofType ext: String) throws
    {
        if let bundlePath = Bundle.main.path(forResource: name, ofType: ext),
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                               .userDomainMask,
                                                               true).first {
            let fileName = "\(name).\(ext)"
            let fullDestPath = URL(fileURLWithPath: destPath)
                .appendingPathComponent(fileName)
            let fullDestPathString = fullDestPath.path
            
            if !self.fileExists(atPath: fullDestPathString) {
                try self.copyItem(atPath: bundlePath, toPath: fullDestPathString)
            }
        }
    }
}

extension DBController {
    func migrateDatabase() {
        if !isExistRecord(query: "select Position from Question limit 1") {
            let rs = excuteUpdate(query: "alter table Question add Position int")
            let rs2 = excuteUpdate(query: "update Question set Position = 10000")
            print(rs)
        }
    }
}

//class TestQuestion: BaseEntity {
//    var t_id = 0
//    var q_id = 0
//    override init(resultSet: FMResultSet) {
//        super.init(resultSet: resultSet)
//        t_id = Int(resultSet.int(forColumn: "t_id"))
//        q_id = Int(resultSet.int(forColumn: "q_id"))
//    }
//}
//
//extension DBController {
//    func getAllTestQuestion(testID: Int) -> [TestQuestion] {
//        var objs = [TestQuestion]()
//        openDB()
//        guard let resultSet = getResultSet(query: "select * from TestQuestion where t_id = \(testID)") else {
//            return []
//        }
//
//        while resultSet.next() {
//            let obj = TestQuestion.init(resultSet: resultSet)
//            objs.append(obj)
//        }
//        closeDB()
//        return objs
//    }
//
//    func updateTestQuestion() {
//        let tests = getAllTest()
//        for i in 0..<tests.count {
//            let j = i + 1
//            let testQuestions = getAllTestQuestion(testID: tests[i].id)
//            switch j {
//            case 1, 16:
//                let videoQuestions = getVideoQuestion(videoID: 1)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 2, 11:
//                let videoQuestions = getVideoQuestion(videoID: 2)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 3, 15:
//                let videoQuestions = getVideoQuestion(videoID: 3)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 6, 17:
//                let videoQuestions = getVideoQuestion(videoID: 4)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 9, 4:
//                let videoQuestions = getVideoQuestion(videoID: 5)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 10, 18:
//                let videoQuestions = getVideoQuestion(videoID: 6)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 13, 7:
//                let videoQuestions = getVideoQuestion(videoID: 7)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 14, 8:
//                let videoQuestions = getVideoQuestion(videoID: 8)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            case 5, 12:
//                let videoQuestions = getVideoQuestion(videoID: 9)
//                let testQuestion48 = testQuestions[47]
//                let testQuestion49 = testQuestions[48]
//                let testQuestion50 = testQuestions[49]
//                let rs1 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[0].id) where t_id = \(tests[i].id) and q_id = \(testQuestion48.q_id)")
//                let rs2 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[1].id) where t_id = \(tests[i].id) and q_id = \(testQuestion49.q_id)")
//                let rs3 = excuteUpdate(query: "update TestQuestion set q_id = \(videoQuestions[2].id) where t_id = \(tests[i].id) and q_id = \(testQuestion50.q_id)")
//                print(rs1, rs2, rs3)
//            default:
//                break
//            }
//        }
//    }
//}
