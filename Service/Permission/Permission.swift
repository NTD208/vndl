//
//  Permission.swift
//  dmv
//
//  Created by Khai Vuong on 01/02/2022.
//

import Foundation
import UserNotifications
import UIKit

class Permission {
    static func requestNotificationPermission(completion: ((_ isSuccess: Bool) -> Void)?) {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { granted, _ in
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    let isSuccesslet = granted
                    completion?(isSuccesslet)
                }
            })
    }
}
