//
//  NSBundleExtensions.swift
//  dmv
//
//  Created by Thom Vu on 23/08/2021.
//

import Foundation
import UIKit

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
}
}
