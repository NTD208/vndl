//
//  ProgressView.swift
//  VNDrivingLicense
//
//  Created by DuNT on 02/04/2022.
//

import UIKit

class ProgressView: UIView {

    var activeView: UIView = UIView()
    var activeContainerView = UIView()
    
    var progress: CGFloat = 0 {
        didSet {
            self.updateLayout()
        }
    }
    
    var color: UIColor = .black {
        didSet {
            self.activeContainerView.backgroundColor = color
        }
    }
    
    var bgColor: UIColor = UIColor(rgb: 0xE8E8E8) {
        didSet {
            self.backgroundColor = bgColor
        }
    }
        
    private var activeViewWidthConstraint: NSLayoutConstraint!
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    private func customInit() {
        backgroundColor = UIColor.init(rgb: 0xE8E8E8)
        addSubview(activeContainerView)
        activeContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        activeViewWidthConstraint = activeContainerView.widthAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            activeContainerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            activeContainerView.topAnchor.constraint(equalTo: topAnchor),
            activeContainerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            activeViewWidthConstraint
        ])
        
        
        activeContainerView.addSubview(activeView)
        activeContainerView.clipsToBounds = true
        activeContainerView.cornerRadius = self.frame.height / 2
        
        cornerRadius = self.frame.height / 2
        
        activeContainerView.backgroundColor = UIColor(named: "PrimaryColor")
    }
    
    private func updateLayout() {
        activeViewWidthConstraint.constant = (progress > 1 ? 1 : progress) * self.frame.width
        self.layoutIfNeeded()
    }

}
