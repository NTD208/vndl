//
//  ProgressCircleView.swift
//  dvsa
//
//  Created by DuNT on 30/12/2021.
//

import UIKit

class ProgressCircleView: UIView {

    var correctColor: UIColor = .white {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var wrongColor: UIColor = .white {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var correctProgress: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var wrongProgress: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var lineWidth: CGFloat = 28 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        let center = CGPoint.init(x: rect.width / 2, y: rect.height)
        
        let startAngleCorrect = CGFloat.pi
        let endAngleCorrect = startAngleCorrect + (CGFloat.pi * correctProgress)
        let startAngleWrong = endAngleCorrect
        let endAngleWrong = startAngleWrong + (CGFloat.pi * wrongProgress)
        // draw Background
        let bgPath = UIBezierPath.init(arcCenter: center, radius: rect.width / 2 - lineWidth/2, startAngle: startAngleCorrect, endAngle: startAngleCorrect + (CGFloat.pi * 2), clockwise: true)
        bgPath.lineWidth = lineWidth
        UIColor.init(rgb: 0xEAEDF3).setStroke()
        bgPath.stroke()
        // draw Progress
        let wrongPath = UIBezierPath.init(arcCenter: center, radius: rect.width / 2  - lineWidth/2, startAngle: startAngleWrong, endAngle: endAngleWrong, clockwise: true)
        wrongPath.lineWidth = lineWidth
        wrongColor.setStroke()
        wrongPath.stroke()
        
        let correctPath = UIBezierPath.init(arcCenter: center, radius: rect.width / 2  - lineWidth/2, startAngle: startAngleCorrect, endAngle: endAngleCorrect, clockwise: true)
        correctPath.lineWidth = lineWidth
        correctColor.setStroke()
        correctPath.stroke()
    }
}
