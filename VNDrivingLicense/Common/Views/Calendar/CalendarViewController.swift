//
//  CalendarViewController.swift
//  dmv
//
//  Created by Khue Minh on 28/06/2021.
//

import UIKit
import FSCalendar

protocol CalendarViewControllerDelegate: AnyObject {
    func calendarViewController(_ controller: CalendarViewController, didSelectDate date: Date)
}

class CalendarViewController: UIViewController {
    weak var delegate: CalendarViewControllerDelegate?
    var selectedDate: Date?
    func show(on owner: UIViewController) {
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
        owner.present(self, animated: true, completion: nil)
    }
    
    @IBOutlet weak var datePickerContainer: UIView!
    
    var datePicker: UIDatePicker!
    func dissmiss() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Variables

    @IBOutlet weak var calendarView: FSCalendar!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.config()
    }

    private func config() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        let datePicker = UIDatePicker.init()
        datePickerContainer.addSubview(datePicker)
        datePicker.fitSuperviewConstraint()
        
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = .date
        datePicker.date = self.selectedDate ?? Date().tomorrow
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.setDate(self.selectedDate ?? Date().tomorrow, animated: false)
        datePicker.locale = Locale(identifier: "vi_VN")
        
        if #available(iOS 14, *) {
            
            
        }
        self.datePicker = datePicker
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        dissmiss()
    }
    
    @IBAction func doneButtonDidTap(_ sender: Any) {
        if datePicker.date <= Date() {
            dissmiss()
            return
        }
        delegate?.calendarViewController(self, didSelectDate: datePicker.date)
        dissmiss()
    }
    
    @IBAction func cancelButtonDidTap(_ sender: Any) {
        dissmiss()
    }
    // MARK:- FSCalendarDataSource
    

    // MARK:- FSCalendarDelegate
    
}
