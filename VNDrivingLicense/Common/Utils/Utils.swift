//
//  Utils.swift
//  dmv
//
//  Created by Thom Vu on 02/06/2021.
//

import Foundation
typealias CompletionBlock = (() -> Void)?
struct DateFormat {
    static let longFormat = "yyyy-MM-dd HH:mm:ss"
    static let shortFormat = "yyyy-MM-dd"
}

struct NotificationName {
    
}

class Utils {
    
    static func stringFromDate(_ date: Date, format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    static func dateFromString(_ string: String, format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: string)
    }
}
