//
//  AppState.swift
//  VNDrivingLicense
//
//  Created by DuNT on 02/04/2022.
//

import Foundation

struct AppQueue {
    static let dbQueue = DispatchQueue.init(label: "dbQueue")
}

struct AppState {
    
    static var completedShowIntro: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "ShowedIntro")
        }
        
        set {
            UserDefaults.standard.setValue(newValue, forKey: "ShowedIntro")
        }
    }
    
    static var testDate: Date? {
        get {
            if let testDateString = UserDefaults.standard.string(forKey: "testDate") {
                return Utils.dateFromString(testDateString, format: DateFormat.shortFormat)
            }
            return nil
        }
        set {
            if let value = newValue, let testDateString = Utils.stringFromDate(value, format: DateFormat.shortFormat) {
                UserDefaults.standard.setValue(testDateString, forKey: "testDate")
            } else {
                UserDefaults.standard.setValue(nil, forKey: "testDate")
            }
        }
    }
    
    static var totalDate: Int? {
        get {
            return UserDefaults.standard.integer(forKey: "totalDate")
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "totalDate")
        }
    }
    
    static var completedRateApp: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "completedRateApp")
        }
        
        set {
            UserDefaults.standard.setValue(newValue, forKey: "completedRateApp")
        }
    }
}
