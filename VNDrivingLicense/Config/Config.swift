//
//  Config.swift
//  VNDrivingLicense
//
//  Created by DuNT on 30/04/2022.
//

import Foundation
import Firebase

private let defaultServerURL = "http://motivation-video.com/storage/DVSA"

struct Config {
    static let AppID = "1613142996"
    
    static var baseURL: String {
        return remoteConfig[ConfigKey.ServerURL].stringValue ?? defaultServerURL
    }
    
    static var remoteConfig: RemoteConfig {
        return Storage.remoteConfig()
    }
}

struct ConfigKey {
    static let ServerURL = "ServerURL"
}

extension RemoteConfig {
    func setDefaultConfig() {
        let defaultConfigs: [String: Any] = [
            ConfigKey.ServerURL : defaultServerURL
        ]
        
        setDefaults(defaultConfigs as? [String: NSObject])
    }
    
    func fetchCloudConfig(completion: CompletionBlock) {
        fetchAndActivate { status, error in
            completion?()
        }
    }
}

private class Storage {
    private static var shared: RemoteConfig?
    static func remoteConfig() -> RemoteConfig {
        if shared == nil {
            shared = RemoteConfig.remoteConfig()
            let settings = RemoteConfigSettings()
            settings.minimumFetchInterval = 0
            shared?.configSettings = settings
        }
        return shared!
    }
}
