//
//  UIColor.swift
//  dmv
//
//  Created by Khai Vuong on 10/02/2022.
//

import Foundation
import UIKit

struct AppColor {
    static let textBlackColor = UIColor(named: "TextBlackColor")!
    static let correctColor = UIColor(named: "PassedColor")!
    static let wrongColor = UIColor(named: "FailedColor")!
    static let backgroundColor = UIColor(named: "AppBackgroundColor")!
    static let primaryColor = UIColor(named: "PrimaryColor")!
}
