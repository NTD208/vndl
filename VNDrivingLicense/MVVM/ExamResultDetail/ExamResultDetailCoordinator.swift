//
//  ExamResultDetailCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 30/04/2022.
//

import UIKit

class ExamResultDetailCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: ExamResultDetailVC?
    var navigation: UINavigationController
    var testResult: TestResult
    var testResultDetails: [TestResultDetail]
    var selectQuestionIndex: Int
    var test: Test
    
    init(navigation: UINavigationController, test: Test ,testResult: TestResult, testResultDetails: [TestResultDetail], selectQuestionIndex: Int) {
        self.navigation = navigation
        self.testResultDetails = testResultDetails
        self.selectQuestionIndex = selectQuestionIndex
        self.testResult = testResult
        self.test = test
    }
    
    func start() {
        if !started {
            started = true
            let controller = ExamResultDetailVC.factory()
            controller.coordinator = self
            controller.selectQuestionIndex = selectQuestionIndex
            controller.testResultDetail = testResultDetails
            controller.testResult = testResult
            controller.test = test
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
