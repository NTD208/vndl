//
//  ExamResultDetailViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 30/04/2022.
//

import UIKit

protocol ExamResultDetailViewModelFactory: BaseViewModelFactory {
    
}

class ExamResultDetailViewModel: ExamResultDetailViewModelFactory {
    
    func appear() {
        
    }
}
