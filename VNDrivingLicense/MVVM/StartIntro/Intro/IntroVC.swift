//
//  IntroVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 26/03/2022.
//

import UIKit

private enum IntroStep {
    case chooseTestDate
    case showOverview
    case requestNotify
}

class IntroVC: BaseViewController<IntroViewModelFactory> {
    var coordinator: IntroCoordinator!
    // MARK: - IBOutlet
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var progressView: ProgressView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var selectedTestDate: Date?
    private var introStep: IntroStep = .chooseTestDate
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        showChooseTestDateIntroStep()
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
        
        configCollectionView()
        configProgressView()
    }
    
    func configCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(type: ChooseTestDateIntroCell.self)
        collectionView.registerCell(type: CourseOverviewCell.self)
        collectionView.registerCell(type: RequestNotificationIntroCell.self)
    }
    
    private func configProgressView() {
        progressView.backgroundColor =  UIColor(rgb: 0xE8E8E8)
        progressView.activeContainerView.backgroundColor = UIColor(named: "PrimaryColor")
        progressView.cornerRadius = 4
        progressView.activeContainerView.cornerRadius = 4
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        showPreviousIntroStep()
    }
    
    
    // MARK: - Helper
    private func showPreviousIntroStep() {
        switch introStep {
        case .showOverview:
            showChooseTestDateIntroStep()
        case .requestNotify:
            showShowOverviewIntroStep()
        default:
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func showChooseTestDateIntroStep() {
        progressView.progress = 1/3
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
        introStep = .chooseTestDate
    }
    
    private func showShowOverviewIntroStep() {
        progressView.progress = 2/3
        collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: true)
        introStep = .showOverview
    }
    
    private func showRequestNotifyIntroStep() {
        progressView.progress = 1
        collectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: .left, animated: true)
        introStep = .requestNotify
    }
}

// MARK: - UICollectionViewDataSource
extension IntroVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            if let cell = collectionView.dequeueCell(type: ChooseTestDateIntroCell.self, indexPath: indexPath) {
                cell.bind(date: self.selectedTestDate, owner: self)
                cell.delegate = self
                return cell
            }
        case 1:
            if let cell = collectionView.dequeueCell(type: CourseOverviewCell.self, indexPath: indexPath) {
                cell.delegate = self
                return cell
            }
        case 2:
            if let cell = collectionView.dequeueCell(type: RequestNotificationIntroCell.self, indexPath: indexPath) {
                cell.delegate = self
                return cell
            }
        default:
            break
        }

        return UICollectionViewCell()
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension IntroVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension IntroVC: ChooseTestDateIntroCellDelegate {
    func chooseTestDateIntroCell(_ cell: ChooseTestDateIntroCell, didTapContinue testDate: Date?) {
        self.selectedTestDate = testDate
        AppState.testDate = testDate
        showShowOverviewIntroStep()
    }
}

extension IntroVC: CourseOverviewCellDelegate {
    func courseOverviewIntrolCellDidTapContinue(_ cell: CourseOverviewCell) {
        showRequestNotifyIntroStep()
    }
}

extension IntroVC: RequestNotifyIntroCellDelegate {
    func requestNotifyIntroCellDidTapContinueButton(_ cell: RequestNotificationIntroCell) {
        AppState.completedShowIntro = true
        if let navigation = self.navigationController {
            let coordinator = HomeCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
}
