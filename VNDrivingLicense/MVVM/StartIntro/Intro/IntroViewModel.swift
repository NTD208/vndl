//
//  IntroViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 26/03/2022.
//

import UIKit

protocol IntroViewModelFactory: BaseViewModelFactory {
    
}

class IntroViewModel: IntroViewModelFactory {
    
    func appear() {
        
    }
}
