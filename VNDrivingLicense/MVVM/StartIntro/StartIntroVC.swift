//
//  StartIntroVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 26/03/2022.
//

import UIKit

class StartIntroVC: BaseViewController<StartIntroViewModelFactory> {
    var coordinator: StartIntroCoordinator!
    // MARK: - IBOutlet
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var startButtonTopConstraint: NSLayoutConstraint!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        let attributedString = NSMutableAttributedString(string: "Bằng lái xe A1")
        attributedString.addAttribute(.foregroundColor, value: UIColor(rgb: 0xF2994A), range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttribute(.foregroundColor, value: AppColor.textBlackColor, range: NSRange(location: 0, length: attributedString.length - 2))
        titleLabel.attributedText = attributedString
        
        let startAttributedString = NSMutableAttributedString(string: "BẮT ĐẦU")
        startAttributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 20, weight: .bold), range: NSRange(location: 0, length: startAttributedString.length))
        startButton.setAttributedTitle(startAttributedString, for: .normal)
        
        startButtonTopConstraint.constant = UIScreen.main.bounds.height > 670 ? 73 : 43
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction
    @IBAction func startButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = IntroCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    
    // MARK: - Helper
}
