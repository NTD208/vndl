//
//  StartIntroViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 26/03/2022.
//

import UIKit

protocol StartIntroViewModelFactory: BaseViewModelFactory {
    
}

class StartIntroViewModel: StartIntroViewModelFactory {
    
    func appear() {
        
    }
}
