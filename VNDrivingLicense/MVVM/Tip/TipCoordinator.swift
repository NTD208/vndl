//
//  TipCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 02/05/2022.
//

import UIKit

class TipCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: TipVC?
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = TipVC.factory()
            controller.coordinator = self
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
