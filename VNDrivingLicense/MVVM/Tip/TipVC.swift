//
//  TipVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 02/05/2022.
//

import UIKit
import ExpyTableView

class TipVC: BaseViewController<TipViewModelFactory> {
    var coordinator: TipCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: ExpyTableView!
    
    // MARK: - Variable
    private var tipDatas = [Tip]()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let path = Bundle.main.path(forResource: "tips", ofType: "json") else { return }
        do {
            let jsonData = try String.init(contentsOfFile: path).data(using: .utf8)
            let decodedData = try JSONDecoder().decode([Tip].self, from: jsonData!)
            self.tipDatas = decodedData
        } catch let error {
            print("error: \(error)")
        }
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
        configTableView()
    }
    
    private func configTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerCell(type: TipExpyTableViewCell.self)
        tableView.registerCell(type: TipTableViewCell.self)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        tableView.expandingAnimation = .fade
        tableView.collapsingAnimation = .fade
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        coordinator.stop()
    }
    
    
    // MARK: - Helper
}

// MARK: - ExpyTableViewDataSource
extension TipVC: ExpyTableViewDataSource {
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(type: TipExpyTableViewCell.self) else {
            return UITableViewCell()
        }
        
        cell.setData(self.tipDatas[section])
        cell.showSeparator()
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tipDatas.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tipDatas[section].tips.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(type: TipTableViewCell.self) else {
            return UITableViewCell()
        }
        
        cell.setData(self.tipDatas[indexPath.section], index: indexPath.row)
//        cell.showSeparator()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

// MARK: - ExpyTableViewDelegate
extension TipVC: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        print(state)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
