//
//  ExamReviewCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 30/04/2022.
//

import UIKit

class ExamReviewCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: ExamReviewVC?
    var presentingVC: UIViewController
    var test: Test
    var testResult: TestResult?
    var testResultDetails: [TestResultDetail]?
    weak var examReviewVCDelegate: ExamReviewVCDelegate?
    
    init(presentingVC: UIViewController, test: Test, testResult: TestResult?, testResultDetails: [TestResultDetail]?, delegate: ExamReviewVCDelegate?) {
        self.presentingVC = presentingVC
        self.test = test
        self.testResult = testResult
        self.testResultDetails = testResultDetails
        self.examReviewVCDelegate = delegate
    }
    
    func start() {
        if !started {
            started = true
            let controller = ExamReviewVC.factory()
            controller.coordinator = self
            controller.test = test
            controller.testResult = testResult
            controller.testResultDetails = testResultDetails
            controller.delegate = examReviewVCDelegate
            
            let nav = UINavigationController.init(rootViewController: controller)
            nav.isNavigationBarHidden = true
            nav.modalPresentationStyle = .fullScreen
            presentingVC.present(nav, animated: true, completion: nil)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            presentingVC.dismiss(animated: false, completion: nil)
        }
    }
}
