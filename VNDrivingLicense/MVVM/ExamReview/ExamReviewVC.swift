//
//  ExamReviewVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 30/04/2022.
//

import UIKit

protocol ExamReviewVCDelegate: AnyObject {
    func testResultVCDidTapClose(_ vc: ExamReviewVC)
    func testResultVCDidTapRetakeButton(test:Test)
}

class ExamReviewVC: BaseViewController<ExamReviewViewModelFactory> {
    var coordinator: ExamReviewCoordinator!
    var testResult: TestResult?
    var testResultDetails: [TestResultDetail]?
    var test: Test!
    weak var delegate: ExamReviewVCDelegate?
    
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressCircleView: ProgressCircleView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var questionsView: UIView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        if testResult == nil {
            viewModel.appear()
        } else {
            self.bindDataToView()
        }
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
    }
    
    func bindViewModel() {
        viewModel.test = test
        viewModel.testResultPassthroughSubject?.combineLatest(viewModel.testResultDetailsPassthroughSubject!).sink(receiveValue: { result in
            self.testResult = result.0
            self.testResultDetails = result.1
            self.bindDataToView()
        }).store(in: &cancellables)
        
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        coordinator.stop()
        delegate?.testResultVCDidTapClose(self)
    }
    
    @IBAction func retakeButtonDidTap(_ sender: Any) {
        coordinator.stop()
        delegate?.testResultVCDidTapRetakeButton(test: self.test)
    }
    
    // MARK: - Helper
    
    private func bindDataToView() {
        scoreLabel.text = "\(testResult?.numberAnswerCorrect ?? 0)/\(testResult?.totalQuestion ?? 0)"
        progressCircleView.correctProgress = CGFloat(testResult?.numberAnswerCorrect ?? 0) / CGFloat(testResult?.totalQuestion ?? 0)
        progressCircleView.wrongProgress = CGFloat((testResult?.totalQuestion ?? 0) - (testResult?.numberAnswerCorrect ?? 0) ) / CGFloat(testResult?.totalQuestion ?? 0)
        progressCircleView.correctColor = AppColor.correctColor
        progressCircleView.wrongColor = AppColor.wrongColor
        if testResult!.isPassed() {
            titleLabel.text = "Chúc mừng!\nBạn đã vượt qua bài thi 🎉"
            scoreLabel.textColor = AppColor.correctColor
        }else {
            titleLabel.text = "Bạn đã trượt bài thi\nHãy luyện tập thêm nhé👏🏼"
            scoreLabel.textColor = AppColor.wrongColor
        }
        
        bindQuestionResult()
        self.view.layoutIfNeeded()
    }
    
    func bindQuestionResult() {
        var previousView: TestResultQuestionView?
        for i in 0..<(testResultDetails?.count ?? 0) {
            let resultQuestionView = TestResultQuestionView()
            resultQuestionView.tag = i
            resultQuestionView.addTarget(self, action: #selector(resultQuestionViewDidTap(_:)), for: .touchUpInside)
            questionsView.addSubview(resultQuestionView)
            resultQuestionView.translatesAutoresizingMaskIntoConstraints = false
            resultQuestionView.setTestResultDetailQuestion(testResultDetails![i], i)


            NSLayoutConstraint.activate([
                resultQuestionView.leadingAnchor.constraint(equalTo: questionsView.leadingAnchor, constant: 16),
                resultQuestionView.trailingAnchor.constraint(equalTo: questionsView.trailingAnchor, constant: -16)
            ])

            if previousView != nil {
                resultQuestionView.topAnchor.constraint(equalTo: previousView!.bottomAnchor, constant: 12).isActive = true
            } else {
                resultQuestionView.topAnchor.constraint(equalTo: questionsView.topAnchor).isActive = true
            }

            previousView = resultQuestionView

            if i == testResultDetails!.count - 1 {
                resultQuestionView.bottomAnchor.constraint(equalTo: questionsView.bottomAnchor, constant: -12).isActive = true
            }
        }
    }
    
    @objc func resultQuestionViewDidTap(_ view: DimableView) {
        if let navigation = self.navigationController, let testResult = testResult, let testResultDetails = testResultDetails {
            let coordinator = ExamResultDetailCoordinator(navigation: navigation, test: test, testResult: testResult, testResultDetails: testResultDetails, selectQuestionIndex: view.tag)
            coordinator.start()
        }
    }
}
