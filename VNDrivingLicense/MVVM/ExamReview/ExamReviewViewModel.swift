//
//  ExamReviewViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 30/04/2022.
//

import UIKit
import Combine

protocol ExamReviewViewModelFactory: BaseViewModelFactory {
    var test: Test! {get set}
    var testResultPassthroughSubject: PassthroughSubject<TestResult, Never>? { get set }
    var testResultDetailsPassthroughSubject: PassthroughSubject<[TestResultDetail], Never>? { get set }
}

class ExamReviewViewModel: ExamReviewViewModelFactory {
    
    var test: Test!
    var testResultPassthroughSubject: PassthroughSubject<TestResult, Never>?
    var testResultDetailsPassthroughSubject: PassthroughSubject<[TestResultDetail], Never>?
    
    init() {
        testResultPassthroughSubject = PassthroughSubject()
        testResultDetailsPassthroughSubject = PassthroughSubject()
    }
    
    func appear() {
        if let testResult = DBController.shared.getTestResult(testID: test.id) {
            testResultPassthroughSubject?.send(testResult)
        }
        let detailQuestions = DBController.shared.getTestResultDetails(testID: test.id)
        testResultDetailsPassthroughSubject?.send(detailQuestions)
    }
}
