//
//  TrafficSignVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit

class TrafficSignVC: BaseViewController<TrafficSignViewModelFactory> {
    var coordinator: TrafficSignCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headMenuView: HeadMenu!
    @IBOutlet weak var headerView: UIView!
    
    // MARK: - Variable
    var signGroups = [SignGroup]()
    var items = [String]()
    var filterTrafficSing: [SignSubGroup]?
    var index = 0
    
    var signSubGroups = [SignSubGroup]()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
        
        headerView.shadowColor = .black
        headerView.shadowRadius = 8
        headerView.shadowOpacity = 0.08
        headerView.shadowOffset = CGSize(width: 0, height: 4)
        
        updateSignsShadow()
        configCollectionView()
        configHeadMenuView()
    }
    
    func configHeadMenuView() {
        headMenuView.delegate = self
        headMenuView.dataSource = self
        headMenuView.collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    func configCollectionView() {
        collectionView.shadowColor = .black
        collectionView.shadowRadius = 12
        collectionView.shadowOffset = CGSize(width: 0, height: 0)
        collectionView.shadowOpacity = 0.08
        
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 10, right: 0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(type: TrafficSignCollectionViewCell.self)
    }
    
    func bindViewModel() {
        viewModel.signGroupsPassThroughSubject?.sink(receiveValue: { signGroups in
            if let group = signGroups.first {
                self.viewModel.selectSignGroup(group)
            }
            self.signGroups = signGroups
            for i in 0..<signGroups.count {
                self.items.append(signGroups[i].groupName)
            }
            self.headMenuView.reloadData()
        }).store(in: &cancellables)
        
        viewModel.signsSubGroupsPassThroughSuject?.sink(receiveValue: { subGroups in
            self.signSubGroups = subGroups
            self.filterTrafficSing = subGroups
            self.collectionView.reloadData()
            self.collectionView.setContentOffset(.zero, animated: false)
        }).store(in: &cancellables)
        
        viewModel.appear()
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        self.coordinator.stop()
    }
    
    
    // MARK: - Helper
    private func updateSignsShadow() {
        headerView.shadowOpacity = collectionView.contentOffset.y > 0 ? 0.08 : 0
    }
    
}

// MARK: - UICollectionViewDataSource
extension TrafficSignVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return filterTrafficSing?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterTrafficSing?[section].signs.count ?? 0

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: TrafficSignCollectionViewCell.self, indexPath: indexPath) else {
            return UICollectionViewCell()
        }
        
        if let group = self.filterTrafficSing?[indexPath.section] {
            cell.bindData(group.signs[indexPath.row])
        }
        
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension TrafficSignVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 20) / 2
        return CGSize.init(width: width, height: 210)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSignsShadow()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let group = self.filterTrafficSing?[indexPath.section] {
            let coordinator = TrafficSignDetailCoordinator(trafficSign: group.signs[indexPath.row], presentingVC: self)
            coordinator.start()
        }
    }
}

// MARK: - HeadMenuDelegate
extension TrafficSignVC: HeadMenuDelegate {
    func headMenu(_ headMenu: HeadMenu, didSelectItemAtIndex index: Int) {
        self.view.endEditing(true)
        viewModel.selectSignGroup(signGroups[index])
        collectionView.setContentOffset(.zero, animated: false)
    }
}

// MARK: - HeadMenuDataSource
extension TrafficSignVC: HeadMenuDataSource {
    func headMenuListItem(_ headMenu: HeadMenu) -> [String] {
        return items
    }
}

extension UIImage {
    func createImageWithRoundBorder(cornerRadiuos : CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, scale)
        let rect = CGRect(origin:CGPoint(x: 0, y: 0), size: self.size)
        let context = UIGraphicsGetCurrentContext()

        let path = UIBezierPath(
            roundedRect: rect,
            cornerRadius: cornerRadiuos
        )
        context?.beginPath()
        context?.addPath(path.cgPath)
        context?.closePath()
        context?.clip()
        self.draw(at: CGPoint.zero)
        context?.restoreGState()
        path.lineWidth = 0
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
