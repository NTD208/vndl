//
//  TrafficSignDetailVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit
import SDWebImage

class TrafficSignDetailVC: BaseViewController<TrafficSignDetailViewModelFactory> {
    var coordinator: TrafficSignDetailCoordinator!
    var sign: Sign!
    // MARK: - IBOutlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        imageView.sd_setImage(with: sign.imageURL())
        titleLabel.text = sign.signCode
        infoLabel.text = sign.title
        descLabel.text = sign.desc
        
        addGesture()
    }
    
    func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func tapGestureAction(_ sender: UITapGestureRecognizer) {
        coordinator.stop(completion: nil)
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction

    
    // MARK: - Helper
}

// MARK: - UIGestureRecognizerDelegate
extension TrafficSignDetailVC: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let location = gestureRecognizer.location(in: self.view)
        let contentFrame = containerView.superview?.convert(containerView.frame, to: self.view) ?? .zero
        if contentFrame.contains(location) {
            return false
        }
        
        return true
    }
}
