//
//  TrafficSignDetailCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit

class TrafficSignDetailCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: TrafficSignDetailVC?
    var presentingVC: UIViewController?
    var trafficSign: Sign
    
    init(trafficSign: Sign, presentingVC: UIViewController?) {
        self.trafficSign = trafficSign
        self.presentingVC = presentingVC
    }
    
    func start() {
        if !started {
            started = true
            let controller = TrafficSignDetailVC.factory()
            controller.coordinator = self
            controller.sign = trafficSign
            
            controller.modalPresentationStyle = .overFullScreen
            controller.modalTransitionStyle = .crossDissolve
            presentingVC?.present(controller, animated: true, completion: nil)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            presentingVC?.dismiss(animated: true, completion: nil)
        }
    }
}
