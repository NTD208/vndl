//
//  SplashVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 26/03/2022.
//

import UIKit

class SplashVC: BaseViewController<SplashViewModelFactory> {
    var coordinator: SplashCoordinator!
    // MARK: - IBOutlet
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        let attributedString = NSMutableAttributedString(string: "Bằng lái xe A1")
        attributedString.addAttribute(.foregroundColor, value: UIColor(rgb: 0xF2994A), range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttribute(.foregroundColor, value: AppColor.textBlackColor, range: NSRange(location: 0, length: attributedString.length - 2))
        titleLabel.attributedText = attributedString
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if let navigation = self.navigationController {
                let coordinator = HomeCoordinator.init(navigation: navigation)
                coordinator.start()
            }
        }
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction

    
    // MARK: - Helper
}
