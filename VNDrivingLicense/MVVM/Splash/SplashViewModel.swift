//
//  SplashViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 26/03/2022.
//

import UIKit

protocol SplashViewModelFactory: BaseViewModelFactory {
    
}

class SplashViewModel: SplashViewModelFactory {
    
    func appear() {
        
    }
}
