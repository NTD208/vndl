//
//  ExamVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 27/04/2022.
//

import UIKit
import ProgressHUD

private struct Const {
    static let timeForNotice = 300
    static let secondPerQuestion = 48
}

class ExamVC: BaseViewController<ExamViewModelFactory> {
    var coordinator: ExamCoordinator!
    var test: Test!
    var selectedAnswers: [Int]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countdownTimeLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var questionControlView: QuestionControlView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var footerContainerView: UIView!
    @IBOutlet weak var footerQuestionControlView: FooterQuestionControlView!
    
    // MARK: - Variable
    private var questions: [Question]?
    var examTime: Int! // second
    var examTimeRemaining: Int!
    var timer: Timer?
    var currentQuestionIndex = 0
    var answeredCount = 0
    private var isScrollingToNextQuestion = false
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.appear()
    }
    
    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        self.startExam()
    }

    // MARK: - Config
    func config() {
        titleLabel.text = self.test.testName
        backButton.setTitle("", for: .normal)
        
        headerView.shadowColor = .black
        headerView.shadowOpacity = 0.08
        headerView.shadowOffset = CGSize(width: 0, height: 4)
        headerView.shadowRadius = 8
        
        configCollectionView()
        questionControlView.delegate = self
        questionControlView.dataSource = self
        
        footerQuestionControlView.shadowColor = .black
        footerQuestionControlView.shadowOpacity = 0.15
        footerQuestionControlView.shadowOffset = CGSize(width: 0, height: -1)
        footerQuestionControlView.shadowRadius = 8
        footerQuestionControlView.delegate = self
    }
    
    func configCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.registerCell(type: AnswerQuestionCell.self)
    }
    
    func bindViewModel() {
        viewModel.test = self.test
        viewModel.questionsPassthroughSubject?.sink(receiveValue: { questions in
            self.examTime = questions.count * Const.secondPerQuestion
            self.examTimeRemaining = self.examTime
            self.updateTimeText()
            self.questions = questions
            self.selectedAnswers = [Int](repeating: 0, count: questions.count)
            self.collectionView.reloadData()
            self.questionControlView.reloadData()
            self.selectedQuestion(index: 0, animated: false)
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        self.stopTimer()
        self.showAlertCancelExam()
    }
    
    @IBAction func submitButtonDidTap(_ sender: Any) {
        self.stopTimer()
        if let selectedAnswers = self.selectedAnswers, selectedAnswers.contains(0) {
            self.showAlertMissing()
        } else {
            self.showAlertConfirmSubmit()
        }
    }
    
    // MARK: - Helper
    func selectedQuestion(index: Int, animated: Bool) {
        currentQuestionIndex = index
        collectionView.setContentOffset(CGPoint.init(x: collectionView.frame.width * CGFloat(index), y: 0), animated: animated)
        questionControlView.selectQuestion(index: index, animated: animated)
        reloadFooterQuestionControlView()
    }
    
    private func showNextQuestion() {
        if isScrollingToNextQuestion {
            return
        }
        
        if self.currentQuestionIndex < self.questions!.count - 1 {
            isScrollingToNextQuestion = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.43) {
                self.selectedQuestion(index: self.currentQuestionIndex + 1, animated: true)
                self.isScrollingToNextQuestion = false
            }
        }
    }
    
    func reloadFooterQuestionControlView() {
        self.footerQuestionControlView.isEnableNextButton = canNextQuestion()
        self.footerQuestionControlView.isEnablePreviousButton = canPresQuestion()
    }
    
    func canNextQuestion() -> Bool {
        return currentQuestionIndex < questions!.count - 1
    }
    
    func canPresQuestion() -> Bool {
        return currentQuestionIndex > 0
    }
    
    func startExam() {
        self.answeredCount = 0
        startTimer()
        
        logDebugStartExam()
    }
    
    private func logDebugStartExam() {
        print("Start Exam, ExamID = \(self.test.id)")
        for i in 0..<self.questions!.count {
            print("\(i + 1) : \(self.questions![i].answer_correct)")
        }
    }
    
    func submitExam() {
        self.stopTimer()
        let result = viewModel.submitExam(testID: test.id, questions: questions!, selectedAnswers: selectedAnswers!)
        let coordinator = ExamReviewCoordinator(presentingVC: self, test: test, testResult: result.testResult, testResultDetails: result.testResultDetails, delegate: self)
        coordinator.start()
    }

    
    func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
            self.examTimeRemaining -= 1
            if self.examTimeRemaining <= 0 {
                self.stopTimer()
                self.showAlertTimeout()
                
            } else if self.examTimeRemaining <= Const.timeForNotice {
                self.countdownTimeLabel.textColor = UIColor(rgb: 0xFE5858)
            }
            
            self.updateTimeText()
        })
    }
    
    func retakeExam() {
        self.examTimeRemaining = self.examTime
        self.updateTimeText()
        self.viewModel.appear()
        self.selectedQuestion(index: 0, animated: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.startExam()
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    func updateTimeText() {
        let second = self.examTimeRemaining % 60
        let minute = (self.examTimeRemaining / 60) % 60
        
        self.countdownTimeLabel.text = String.init(format: "%02ld:%02ld", minute, second)
    }
    
    func showAlertTimeout() {
        let alertController = UIAlertController.init(title: "", message: "You have run out of time.", preferredStyle: .alert)
        let action = UIAlertAction.init(title: "Submit", style: .default) { _ in
            self.submitExam()
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertConfirmSubmit() {
        let alertController = UIAlertController.init(title: "", message: "You have finished the test. Do you want to submit?", preferredStyle: .alert)
        
        let action = UIAlertAction.init(title: "Not yet", style: .default) { [weak self] _ in
            self?.startTimer()
        }
        
        let submitAction = UIAlertAction.init(title: "Submit", style: .default) {  [weak self] _ in
            self?.submitExam()
        }
        
        alertController.addAction(action)
        alertController.addAction(submitAction)
        alertController.preferredAction = submitAction
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertMissing() {
        guard let numberAnswered = self.selectedAnswers?.filter({$0 != 0}) else {
            return
        }
        
        let alertController = UIAlertController.init(title: "", message: "You have only answer \(numberAnswered.count)/\(questions!.count).\nDo you want to submit?", preferredStyle: .alert)
        
        let action = UIAlertAction.init(title: "Not yet", style: .default) { [weak self] _ in
            self?.startTimer()
        }
        
        let submitAction = UIAlertAction.init(title: "Submit", style: .default) { [weak self] _ in
            self?.submitExam()
        }
        
        alertController.addAction(action)
        alertController.addAction(submitAction)
        alertController.preferredAction = submitAction
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertCancelExam() {
        let alertController = UIAlertController.init(title: "", message: "Caution! Your result won't be saved. Do you want to leave the test?", preferredStyle: .alert)
        let yesAction = UIAlertAction.init(title: "Exit", style: .default) { [weak self] _ in
            self?.stopTimer()
            self?.coordinator.stop()
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { [weak self] _ in
            self?.startTimer()
        }
        
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        
        
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewDataSource
extension ExamVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.questions?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: AnswerQuestionCell.self, indexPath: indexPath), let question = self.questions?[indexPath.row] else { return UICollectionViewCell() }
        
        cell.setQuestion(question, questionIndex: indexPath.row)
        cell.showSelectAnswer()
        cell.delegate = self
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExamVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        finishedScroll(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        finishedScroll(scrollView)
    }
    
    private func finishedScroll(_ scrollView: UIScrollView) {
        currentQuestionIndex = Int(scrollView.contentOffset.x / scrollView.frame.width)
        questionControlView.selectQuestion(index: currentQuestionIndex, animated: true)
        reloadFooterQuestionControlView()
    }
}

// MARK: - AnswerCellDelegate
extension ExamVC: AnswerCellDelegate {
    func answerCell(_ cell: AnswerCell, selectAnswer answer: Int) {
        if let question = cell.currentQuestion(), let index = collectionView.indexPath(for: cell as! UICollectionViewCell)?.row {
            question.selectedAnswer = answer
            self.selectedAnswers![index] = answer
            cell.showSelectAnswer()
            self.questionControlView.reloadItem(at: index)
            self.showNextQuestion()
        }
    }
}

// MARK: - QuestionControlViewDelegate
extension ExamVC: QuestionControlViewDelegate {
    func questionControlView(_ view: QuestionControlView, didSelectQuestionIndex index: Int) {
        currentQuestionIndex = index
        view.selectQuestion(index: index, animated: true)
        view.reloadData()
        collectionView.scrollToItem(at: IndexPath.init(row: index, section: 0), at: .left, animated: false)
        reloadFooterQuestionControlView()
    }
}

// MARK: - QuestionControlViewDataSource
extension ExamVC: QuestionControlViewDataSource {
    func questionControlViewNumberOfQuestion(_ view: QuestionControlView) -> Int {
        return self.questions?.count ?? 0
    }
    
    func questionControlView(_ view: QuestionControlView, textColorForItemAt index: Int) -> UIColor {
        if let question = questions?[index] {
            return question.selectedAnswer > 0 ? .white : .black
        }
        return .clear
    }
    
    func questionControlView(_ view: QuestionControlView, borderColorForItemAt index: Int) -> UIColor? {
        return index == currentQuestionIndex ? UIColor(rgb: 0xF2994A) : nil
    }
    
    func questionControlView(_ view: QuestionControlView, backgroundColorForItemAt index: Int) -> UIColor {
        if let question = questions?[index] {
            return question.selectedAnswer > 0 ? AppColor.primaryColor : .clear
        }
        return .clear
    }
}

// MARK: - FooterQuestionControlViewDelegate
extension ExamVC: FooterQuestionControlViewDelegate {
    func footerQuestionControlViewDidTapNextButton(_ view: FooterQuestionControlView) {
        self.selectedQuestion(index: currentQuestionIndex + 1, animated: true)
    }
    
    func footerQuestionControlViewDidTapPreviousButton(_ view: FooterQuestionControlView) {
        self.selectedQuestion(index: currentQuestionIndex - 1, animated: true)
    }
}

// MARK: - ExamReviewVCDelegate
extension ExamVC: ExamReviewVCDelegate {
    func testResultVCDidTapClose(_ vc: ExamReviewVC) {
        coordinator.stop()
    }
    
    func testResultVCDidTapRetakeButton(test: Test) {
        self.retakeExam()
    }
}
