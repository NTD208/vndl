//
//  TestQuestion.swift
//  VNDrivingLicense
//
//  Created by DuNT on 25/03/2022.
//

import UIKit
import FMDB

class TestQuestion: BaseEntity {
    var t_id = 0
    var q_id = 0
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.t_id = Int(resultSet.int(forColumn: "t_id"))
        self.q_id = Int(resultSet.int(forColumn: "q_id"))
    }
}
