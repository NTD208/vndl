//
//  TestResult.swift
//  VNDrivingLicense
//
//  Created by DuNT on 27/04/2022.
//

import UIKit
import FMDB

class TestResult: BaseEntity {
    var testID: Int!
    var numberAnswerCorrect: Int!
    var numberAnswerIncorrect: Int!
    var totalQuestion: Int!
    var miniumAnswerCorrect: Int!
    var testTime: String!
    
    var testResultDetails: [TestResultDetail]?
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.testID = Int(resultSet.int(forColumn: "testID"))
        self.numberAnswerCorrect = Int(resultSet.int(forColumn: "numberAnswerCorrect"))
        self.numberAnswerIncorrect = Int(resultSet.int(forColumn: "numberAnswerIncorrect"))
        self.totalQuestion = Int(resultSet.int(forColumn: "totalQuestion"))
        self.miniumAnswerCorrect = Int(resultSet.int(forColumn: "miniumAnswerCorrect"))
        self.testTime = resultSet.string(forColumn: "testTime")
    }
    
    override func saveUpdate() {
        super.saveUpdate()
        if DBController.shared.isExistRecord(query: "select 1 from TestResult where testID = \(self.testID!) limit 1") {
            let query = "update TestResult set numberAnswerCorrect = \(self.numberAnswerCorrect!), numberAnswerIncorrect = \(self.numberAnswerIncorrect!), testTime = '\(self.testTime!)' where testID = \(self.testID!)"
            _ = DBController.shared.excuteUpdate(query: query)
        } else  {
            insertToDatabase()
        }
    }
    
    override func insertToDatabase() {
        let query = "insert into TestResult (testID, numberAnswerCorrect, miniumAnswerCorrect, totalQuestion, testTime, numberAnswerIncorrect) values (\(self.testID!), \(numberAnswerCorrect!), \(self.miniumAnswerCorrect!), \(self.totalQuestion!), '\(testTime!)', \(self.numberAnswerIncorrect!))"
        _ = DBController.shared.excuteUpdate(query: query)
    }
    
    func isPassed() -> Bool {
        return numberAnswerCorrect >= miniumAnswerCorrect
    }
}
