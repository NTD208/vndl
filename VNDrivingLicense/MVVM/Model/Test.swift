//
//  Test.swift
//  VNDrivingLicense
//
//  Created by DuNT on 25/03/2022.
//

import UIKit
import FMDB

class Test: BaseEntity {
    var id = 0
    var testName = ""
    
    var testResult: TestResult?
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.id = Int(resultSet.int(forColumn: "id"))
        self.testName = resultSet.string(forColumn: "testName") ?? ""
    }
}
