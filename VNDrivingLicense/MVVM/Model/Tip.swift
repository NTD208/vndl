//
//  Tip.swift
//  VNDrivingLicense
//
//  Created by DuNT on 02/05/2022.
//

import Foundation

struct Tip: Codable {
    let type: Int
    let name: String
    let tips: [String]
}
