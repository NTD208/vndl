//
//  QuestionType.swift
//  VNDrivingLicense
//
//  Created by DuNT on 25/03/2022.
//

import UIKit
import FMDB

class QuestionType: BaseEntity, NSCopying {
    var id = 0
    var title = ""
    var desc = ""
    var totalQuestion = 0
    var imageName = ""
    var lastPracticeTime = ""
    var correctCount = 0
    var incorrectCount = 0
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.id = Int(resultSet.int(forColumn: "id"))
        self.title = resultSet.string(forColumn: "title") ?? ""
        self.desc = resultSet.string(forColumn: "desc") ?? ""
        self.totalQuestion = Int(resultSet.int(forColumn: "totalQuestion"))
        self.imageName = resultSet.string(forColumn: "imageName") ?? ""
        self.lastPracticeTime = resultSet.string(forColumn: "lastPracticeTime") ?? ""
        self.correctCount = Int(resultSet.int(forColumn: "correctCount"))
        self.incorrectCount = Int(resultSet.int(forColumn: "inCorrectCount"))
    }
    
    override func saveUpdate() {
        super.saveUpdate()
        let query = "update QuestionType set lastPracticeTime = '\(lastPracticeTime)', correctCount = \(correctCount), inCorrectCount = \(incorrectCount) where id = \(self.id)"
        _ = DBController.shared.excuteUpdate(query: query)
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copyObject = QuestionType()
        copyObject.id = id
        copyObject.title = title
        copyObject.desc = desc
        copyObject.imageName = imageName
        copyObject.totalQuestion = totalQuestion
        copyObject.lastPracticeTime = lastPracticeTime
        copyObject.correctCount = correctCount
        copyObject.incorrectCount = incorrectCount
        return copyObject
    }
}
