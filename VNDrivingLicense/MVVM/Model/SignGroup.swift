//
//  SignGroup.swift
//  VNDrivingLicense
//
//  Created by DuNT on 26/03/2022.
//

import UIKit
import FMDB

class SignGroup: BaseEntity {
    var id = 0
    var groupName = ""
    var signs: [Sign]?
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.id = Int(resultSet.int(forColumn: "id"))
        self.groupName = resultSet.string(forColumn: "groupName") ?? ""
    }
}
