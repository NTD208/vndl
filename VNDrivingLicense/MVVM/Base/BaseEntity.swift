//
//  BaseEntity.swift
//  dmv
//
//  Created by Thom Vu on 25/05/2021.
//

import Foundation
import FMDB

class BaseEntity: NSObject {
    override init() {
        super.init()
    }
    
    init(resultSet: FMResultSet) {
        
    }
    
    func insertToDatabase() {
        
    }
    
    func saveUpdate() {
        
    }
}
