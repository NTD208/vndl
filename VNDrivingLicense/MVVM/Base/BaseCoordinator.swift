//
//  BaseCoordinator.swift
//  dmv
//
//  Created by Thom Vu on 25/05/2021.
//

import Foundation

protocol BaseCoordinator {
    func start()
}
