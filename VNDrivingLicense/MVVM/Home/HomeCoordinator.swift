//
//  HomeCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 03/04/2022.
//

import UIKit

class HomeCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: HomeVC?
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = HomeVC.factory()
            controller.coordinator = self
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
