//
//  HomeVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 03/04/2022.
//

import UIKit

class HomeVC: BaseViewController<HomeViewModelFactory> {
    var coordinator: HomeCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    
    // TestDate
    @IBOutlet weak var setTestDateView: UIView!
    @IBOutlet weak var setTestDateButton: UIButton!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var testDateView: UIView!
    @IBOutlet weak var dayleftNumberLabel: UILabel!
    @IBOutlet weak var testDateLabel: UILabel!
    @IBOutlet weak var progressTestDateView: UIView!
    @IBOutlet weak var progressTestDateWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var clockImageView: UIImageView!
    @IBOutlet weak var clockImageViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var clockImageViewCenterXConstraint: NSLayoutConstraint!
    
    // Sign
    @IBOutlet weak var signButton: UIButton!
    
    //Tip
    @IBOutlet weak var tipButton: UIButton!
    
    // Practice
    @IBOutlet weak var numberQuestionCorrectLabel: UILabel!
    @IBOutlet weak var numberQuestionInCorrectLabel: UILabel!
    @IBOutlet weak var numberQuestionRemainingLabel: UILabel!
    @IBOutlet weak var pieChart: PieChart!
    @IBOutlet weak var totalQuestionLabel: UILabel!
    
    //Test
    @IBOutlet weak var numberTestDoneLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var progressGradientView: ProgressGradientView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        self.view.layoutIfNeeded()
        headerView.roundView(coners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateProgressTestDate(testDate: AppState.testDate, totalDate: AppState.totalDate)
        updateUIForTestDate(AppState.testDate)
        viewModel.appear()
        view.layoutIfNeeded()
    }

    // MARK: - Config
    func config() {
        settingButton.setTitle("", for: .normal)
        settingButton.setImage(UIImage(named: "ic_setting")?.withRenderingMode(.alwaysOriginal), for: .normal)
        
        progressTestDateView.cornerRadius = 19/2
        
        let attributedString = NSMutableAttributedString(string: "Thiết lập ngày thi của bạn")
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 12, weight: .semibold), range: NSRange(location: 0, length: attributedString.length))
        setTestDateButton.setAttributedTitle(attributedString, for: .normal)
        
        let signAttributedString = NSMutableAttributedString(string: "Biển báo")
        signAttributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 16, weight: .semibold), range: NSRange(location: 0, length: signAttributedString.length))
        signButton.setAttributedTitle(signAttributedString, for: .normal)
        
        let tipAttributedString = NSMutableAttributedString(string: "Mẹo thi")
        tipAttributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 16, weight: .semibold), range: NSRange(location: 0, length: tipAttributedString.length))
        tipButton.setAttributedTitle(tipAttributedString, for: .normal)
        
        progressGradientView.gradientColors = [UIColor(rgb: 0xFFD4AE).cgColor, UIColor(rgb: 0xF2994A).cgColor]
        
        NotificationCenter.default.addObserver(self, selector: #selector(receviedData), name: NSNotification.Name("newDataToLoad"), object: nil)
    }
    
    func bindViewModel() {
        viewModel.allQuestionPassThroughSubject?.sink(receiveValue: { questions in
            let attributed1 = NSMutableAttributedString(string: "\(questions.count)\n")
            attributed1.addAttribute(.font, value: UIFont.systemFont(ofSize: 17, weight: .semibold), range: NSRange(location: 0, length: attributed1.length))
            
            let attributed2 = NSMutableAttributedString(string: "Câu hỏi")
            attributed2.addAttribute(.font, value: UIFont.systemFont(ofSize: 12, weight: .medium), range: NSRange(location: 0, length: attributed2.length))
            
            attributed1.append(attributed2)
            self.totalQuestionLabel.attributedText = attributed1
            
            let numberCorrect = questions.filter{($0.practicePass())}.count
            let numberIncorrect = questions.filter{(!($0.practicePass()) && $0.practice_answer != 0)}.count
            let numberRemaining = questions.filter{($0.practice_answer == 0)}.count
            
            self.numberQuestionCorrectLabel.text = "\(numberCorrect)"
            self.numberQuestionInCorrectLabel.text = "\(numberIncorrect)"
            self.numberQuestionRemainingLabel.text = "\(numberRemaining)"
            
            self.pieChart.correctProgress = CGFloat(numberCorrect) / CGFloat(questions.count)
            self.pieChart.wrongProgress = CGFloat(numberIncorrect) / CGFloat(questions.count)
            self.pieChart.correctColor = AppColor.correctColor
            self.pieChart.wrongColor = AppColor.wrongColor
        }).store(in: &cancellables)
        
        viewModel.allTestPassThroughSubject?.sink(receiveValue: { tests in
            let numberTestDone = tests.filter({$0.testResult != nil}).count
            let totalTest = tests.count
            let totalScore = tests.count * 25
            var totalScoreDone = 0
            tests.filter({$0.testResult != nil}).forEach { test in
                totalScoreDone += test.testResult!.numberAnswerCorrect
            }
            
            self.numberTestDoneLabel.text = "\(numberTestDone)/\(totalTest)"
            self.scoreLabel.text = "\(totalScoreDone)/\(totalScore)"
            self.progressGradientView.progress = CGFloat(numberTestDone) / CGFloat(totalTest)
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction
    @IBAction func setTestDateButtonDidTap(_ sender: Any) {
        let calendarVC = CalendarViewController.init()
        calendarVC.delegate = self
        calendarVC.selectedDate = AppState.testDate
        calendarVC.show(on: self)
    }
    
    @IBAction func trafficSignButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = TrafficSignCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    @IBAction func tipButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = TipCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    @IBAction func practiceButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = PracticeOverviewCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    @IBAction func examButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = ExamOverviewCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    @IBAction func settingButtonDidTap(_ sender: Any) {
        let coordinator = SettingCoordinator(presentingVC: self)
        coordinator.start()
    }
    
    
    // MARK: - Helper
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func updateUIForTestDate(_ date: Date?) {
        if let testDate = AppState.testDate, testDate > Date().yesterDay {
            
        } else {
            
        }
        
        if let date = date, (date > Date() || date.isInToday) {
            let dayNumber = date.dayNumber(from: Date())
            setTestDateView.isHidden = true
            dayleftNumberLabel.text = "\(dayNumber)"
            testDateLabel.text = "\(Utils.stringFromDate(date, format: "dd/MM/yyyy")!)"
            
            testDateView.isHidden = date.isInToday
            todayView.isHidden = !date.isInToday
        } else {
            progressTestDateWidthConstraint.constant = clockImageView.frame.width / 2
            dayleftNumberLabel.text = ""
            testDateView.isHidden = true
            setTestDateView.isHidden = false
        }
    }
    
    func updateProgressTestDate(testDate: Date?, totalDate: Int?) {
        guard let testDate = testDate, let totalDate = totalDate else { return }
        self.progressTestDateView.backgroundColor = UIColor.white
        
        let dayNumber = testDate.dayNumber(from: Date())
        
        let percent = (CGFloat(totalDate) - CGFloat(dayNumber)) / CGFloat(totalDate)
        
        if percent <= 0 {
            progressTestDateWidthConstraint.constant = clockImageView.frame.width - 10
        } else if percent == 1 {
            clockImageViewCenterXConstraint.priority = UILayoutPriority(250)
            clockImageViewTrailingConstraint.priority = UILayoutPriority(1000)
            progressTestDateWidthConstraint.constant = progressTestDateView.frame.width
        } else {
            progressTestDateWidthConstraint.constant = progressTestDateView.frame.width * percent + clockImageView.frame.width
        }
        view.layoutIfNeeded()
    }
    
    @objc func receviedData() {
        updateProgressTestDate(testDate: AppState.testDate, totalDate: AppState.totalDate)
        updateUIForTestDate(AppState.testDate)
    }
}

// MARK: - CalendarViewControllerDelegate
extension HomeVC: CalendarViewControllerDelegate {
    func canSelectDate(_ date: Date) -> Bool {
        return date > Date()
    }
    
    func calendarViewController(_ controller: CalendarViewController, didSelectDate date: Date) {
        AppState.totalDate = date.dayNumber(from: Date())
        AppState.testDate = date
        updateUIForTestDate(date)
    }
}
