//
//  PracticeVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 24/04/2022.
//

import UIKit

class PracticeVC: BaseViewController<PracticeViewModelFactory> {
    var coordinator: PracticeCoordinator!
    var questionType: QuestionType!
    // MARK: - IBOutlet
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var questionControlView: QuestionControlView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var footerContainerView: UIView!
    @IBOutlet weak var footerQuestionControlView: FooterQuestionControlView!
    
    // MARK: - Variables
    private var questions: [Question]?
    private var selectedQuestionIndex = 0
    private var oldQuestions: [Question]?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        
        view.layoutIfNeeded()
        viewModel.appear()
    }

    // MARK: - Config
    func config() {
        titleLabel.text = questionType.title
        backButton.setTitle("", for: .normal)
        
        headerView.shadowColor = .black
        headerView.shadowOpacity = 0.08
        headerView.shadowOffset = CGSize(width: 0, height: 4)
        headerView.shadowRadius = 8
        
        configCollectionView()
        questionControlView.delegate = self
        questionControlView.dataSource = self
        
        footerQuestionControlView.shadowColor = .black
        footerQuestionControlView.shadowOpacity = 0.15
        footerQuestionControlView.shadowOffset = CGSize(width: 0, height: -1)
        footerQuestionControlView.shadowRadius = 8
        footerQuestionControlView.delegate = self
    }
    
    func configCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.registerCell(type: AnswerQuestionCell.self)
    }
    
    func bindViewModel() {
        viewModel.questionType = self.questionType
        viewModel.questionsPassthroughSubject?.sink(receiveValue: { questions in
            self.questions = questions
            self.oldQuestions = questions.map({ obj in
                obj.copy() as! Question
            })
            self.questions?.forEach({$0.practice_answer = 0})
            self.questionControlView.reloadData()
            self.collectionView.reloadData()
            self.selectedQuestionIndex = self.oldQuestions!.firstIndex(where: {$0.practice_answer == 0}) ?? 0
            self.selectedQuestion(index: self.selectedQuestionIndex, animated: false)
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        self.coordinator.stop()
    }
    
    
    // MARK: - Helper
    func selectedQuestion(index: Int, animated: Bool) {
        selectedQuestionIndex = index
        collectionView.setContentOffset(CGPoint.init(x: collectionView.frame.width * CGFloat(index), y: 0), animated: animated)
        questionControlView.selectQuestion(index: index, animated: animated)
        reloadFooterQuestionControlView()
    }
    
    func reloadFooterQuestionControlView() {
        self.footerQuestionControlView.isEnableNextButton = canNextQuestion()
        self.footerQuestionControlView.isEnablePreviousButton = canPresQuestion()
    }
    
    func canNextQuestion() -> Bool {
        return selectedQuestionIndex < questions!.count - 1
    }
    
    func canPresQuestion() -> Bool {
        return selectedQuestionIndex > 0
    }
}

// MARK: - UICollectionViewDataSource
extension PracticeVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.questions?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: AnswerQuestionCell.self, indexPath: indexPath), let question = self.questions?[indexPath.row] else { return UICollectionViewCell() }
        
        cell.setQuestion(question, questionIndex: indexPath.row)
        cell.delegate = self
        if question.practice_answer != 0 {
            cell.showResultPractice()
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PracticeVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        finishedScroll(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        finishedScroll(scrollView)
    }
    
    private func finishedScroll(_ scrollView: UIScrollView) {
        selectedQuestionIndex = Int(scrollView.contentOffset.x / scrollView.frame.width)
        questionControlView.selectQuestion(index: selectedQuestionIndex, animated: true)
        reloadFooterQuestionControlView()
    }
}

// MARK: - AnswerCellDelegate
extension PracticeVC: AnswerCellDelegate {
    func answerCell(_ cell: AnswerCell, selectAnswer answer: Int) {
        if let question = cell.currentQuestion(), question.practice_answer == 0, let indexPath = collectionView.indexPath(for: cell as! UICollectionViewCell) {
            question.practice_answer = answer
            oldQuestions?[indexPath.row].practice_answer = answer
            cell.showResultPractice(afterTime: 0.5)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.questionControlView.reloadItem(at: indexPath.row)
            }
            
            viewModel.savePracticeHistory(question)
        }
    }
}

// MARK: - QuestionControlViewDelegate
extension PracticeVC: QuestionControlViewDelegate {
    func questionControlView(_ view: QuestionControlView, didSelectQuestionIndex index: Int) {
        selectedQuestionIndex = index
        view.selectQuestion(index: index, animated: true)
        view.reloadData()
        collectionView.scrollToItem(at: IndexPath.init(row: index, section: 0), at: .left, animated: false)
        reloadFooterQuestionControlView()
    }
}

// MARK: - QuestionControlViewDataSource
extension PracticeVC: QuestionControlViewDataSource {
    func questionControlViewNumberOfQuestion(_ view: QuestionControlView) -> Int {
        return self.questions?.count ?? 0
    }
    
    func questionControlView(_ view: QuestionControlView, textColorForItemAt index: Int) -> UIColor {
        if let question = oldQuestions?[index] {
            return question.practice_answer > 0 ? .white : .black
        }
        return .clear
    }
    
    func questionControlView(_ view: QuestionControlView, borderColorForItemAt index: Int) -> UIColor? {
        return index == selectedQuestionIndex ? UIColor(rgb: 0xF2994A) : nil
    }
    
    func questionControlView(_ view: QuestionControlView, backgroundColorForItemAt index: Int) -> UIColor {
        if let question = oldQuestions?[index] {
            if question.practice_answer == 0 {
                return .white
            } else if question.practicePass() {
                return AppColor.correctColor
            } else {
                return AppColor.wrongColor
            }
        }
        
        return .clear
    }
    
    
}

// MARK: - FooterQuestionControlViewDelegate
extension PracticeVC: FooterQuestionControlViewDelegate {
    func footerQuestionControlViewDidTapNextButton(_ view: FooterQuestionControlView) {
        self.selectedQuestion(index: selectedQuestionIndex + 1, animated: true)
    }
    
    func footerQuestionControlViewDidTapPreviousButton(_ view: FooterQuestionControlView) {
        self.selectedQuestion(index: selectedQuestionIndex - 1, animated: true)
    }
}
