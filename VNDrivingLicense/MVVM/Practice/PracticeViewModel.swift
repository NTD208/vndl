//
//  PracticeViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 24/04/2022.
//

import UIKit
import Combine

protocol PracticeViewModelFactory: BaseViewModelFactory {
    var questionType: QuestionType! {get set}
    var questionsPassthroughSubject: PassthroughSubject <[Question], Never>? {get set}
    func savePracticeHistory(_ question: Question)
}

class PracticeViewModel: PracticeViewModelFactory {
    
    var questionType: QuestionType!
    var questionsPassthroughSubject: PassthroughSubject <[Question], Never>?
    
    init() {
        questionsPassthroughSubject = PassthroughSubject()
    }
    
    func appear() {
        let questions = questionType.id != 7 ? DBController.shared.getListQuestion(questionTypeID: questionType.id) : DBController.shared.getAllQuestion()
        questionsPassthroughSubject?.send(questions)
    }
    
    func savePracticeHistory(_ question: Question) {
        question.saveUpdate()
        let questionType = self.questionType.copy() as! QuestionType
        let questions = questionType.id != 7 ? DBController.shared.getListQuestion(questionTypeID: questionType.id) : DBController.shared.getAllQuestion()
        questionType.lastPracticeTime = Utils.stringFromDate(Date(), format: DateFormat.longFormat) ?? ""
        questionType.correctCount = questions.filter({$0.practicePass()}).count
        questionType.incorrectCount = questions.filter({$0.practice_answer > 0 && $0.practicePass() == false}).count
        questionType.saveUpdate()
    }
}
