//
//  PracticeCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 24/04/2022.
//

import UIKit

class PracticeCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: PracticeVC?
    var navigation: UINavigationController
    var questionType: QuestionType
    
    init(navigation: UINavigationController, questionType: QuestionType) {
        self.navigation = navigation
        self.questionType = questionType
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = PracticeVC.factory()
            controller.coordinator = self
            controller.questionType = questionType
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
