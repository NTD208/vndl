//
//  SettingVC.swift
//  dvsa
//
//  Created by vietlv on 31/12/2021.
//

import UIKit
import StoreKit

class SettingVC: BaseViewController<SettingViewModelFactory> {
    var coordinator: SettingCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var testDateLabel: UILabel!
    @IBOutlet weak var premiumView: DimableView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUIForTestDate(date: AppState.testDate)
//        if PremiumUser.isPremium {
//            premiumView.isHidden = true
//        }
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
        backButton.setImage(UIImage(named: "ic_close")?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
    
    func bindViewModel() {
        
    }
    
    func updateUIForTestDate(date: Date?) {
        if let date = date, (date > Date() || date.isInToday) {
            testDateLabel.text = "\(Utils.stringFromDate(date, format: "dd/MM/yyyy")!)"
        } else {
            testDateLabel.text = ""
        }
        
    }
    
    // MARK: - IBAction
   
    @IBAction func premiumDidTap(_ sender: Any) {
//        let coordinator = IAPCoordinator(presenting: self, inappSource: .buttonVipSetting, delegate: self)
//        coordinator.start()
    }
    
    @IBAction func backButtonDidTap(_ sender: Any) {
        self.coordinator.stop()
    }
    
    @IBAction func testDayDidTap(_ sender: Any) {
        let calendarVC = CalendarViewController.init()
        calendarVC.delegate = self
        calendarVC.selectedDate = AppState.testDate
        calendarVC.show(on: self)
    }
    
    @IBAction func shareDidTap(_ sender: Any) {
        let appURL = URL(string: "https://apps.apple.com/us/app/id\(Config.AppID)")!
        let vc = UIActivityViewController(activityItems: [appURL], applicationActivities: nil)
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func rateUsDidTap(_ sender: Any) {
        AppState.completedRateApp = true
        SKStoreReviewController.requestReview()
    }
    
    // MARK: - Helper
}

extension SettingVC: CalendarViewControllerDelegate {
    func calendarViewController(_ controller: CalendarViewController, didSelectDate date: Date) {
        AppState.totalDate = date.dayNumber(from: Date())
        AppState.testDate = date
        updateUIForTestDate(date: date)
                
        NotificationCenter.default.post(name: NSNotification.Name("newDataToLoad"), object: nil)
    }
    
    func canSelectDate(_ date: Date) -> Bool {
        return date > Date()
    }
    
}

//extension SettingVC: IAPDelegate {
//    func iapSubDidTapBackButton(_ cell: IAPVC, _ inappSource: InappSource) {
//
//    }
//}
