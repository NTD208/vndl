//
//  SettingCoordinator.swift
//  dvsa
//
//  Created by vietlv on 31/12/2021.
//

import UIKit

class SettingCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: SettingVC?
    var presentingVC: UIViewController?
    
    init(presentingVC: UIViewController?) {
        self.presentingVC = presentingVC
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = SettingVC.factory()
            controller.coordinator = self
            
           // navigation.pushViewController(controller, animated: true)
            let nav = UINavigationController.init(rootViewController: controller)
            nav.isNavigationBarHidden = true
            presentingVC?.present(nav, animated: true, completion: nil)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
          //  navigation.popViewController(animated: true)
            presentingVC?.dismiss(animated: true, completion: nil)
        }
    }
}
