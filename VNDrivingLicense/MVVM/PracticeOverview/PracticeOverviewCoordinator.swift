//
//  PracticeOverviewCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit

class PracticeOverviewCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: PracticeOverviewVC?
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = PracticeOverviewVC.factory()
            controller.coordinator = self
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
