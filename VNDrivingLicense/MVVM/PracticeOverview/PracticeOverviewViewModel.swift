//
//  PracticeOverviewViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit
import Combine

protocol PracticeOverviewViewModelFactory: BaseViewModelFactory {
    var questionsTypePassthroughSubject: PassthroughSubject<[QuestionType], Never>? {get set}
}

class PracticeOverviewViewModel: PracticeOverviewViewModelFactory {
    
    var questionsTypePassthroughSubject: PassthroughSubject<[QuestionType], Never>?
    
    init() {
        questionsTypePassthroughSubject = PassthroughSubject()
    }
    
    func appear() {
        let questionsTpye = DBController.shared.getlistQuestionType()
        questionsTypePassthroughSubject?.send(questionsTpye)
    }
}
