//
//  PracticeOverviewVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit

class PracticeOverviewVC: BaseViewController<PracticeOverviewViewModelFactory> {
    var coordinator: PracticeOverviewCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variable
    private var questionsType = [QuestionType]()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.appear()
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
        
        configCollectionView()
    }
    
    func configCollectionView() {
        tableView.shadowColor = .black
        tableView.shadowOpacity = 0.08
        tableView.shadowOffset = CGSize(width: 0, height: 2)
        tableView.shadowRadius = 8
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.registerCell(type: PracticeOverviewCell.self)
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
    }
    
    func bindViewModel() {
        viewModel.questionsTypePassthroughSubject?.sink(receiveValue: { questionsType in
            self.questionsType = questionsType
            self.tableView.reloadData()
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        self.coordinator.stop()
    }
    
    
    // MARK: - Helper
}

// MARK: - UITableViewDataSource
extension PracticeOverviewVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questionsType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let  cell = tableView.dequeueCell(type: PracticeOverviewCell.self) else { return UITableViewCell() }
        cell.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.estimatedRowHeight)
        cell.bindData(self.questionsType[indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate
extension PracticeOverviewVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let navigation = self.navigationController {
            let coordinator = PracticeCoordinator(navigation: navigation, questionType: self.questionsType[indexPath.row])
            coordinator.start()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
}
