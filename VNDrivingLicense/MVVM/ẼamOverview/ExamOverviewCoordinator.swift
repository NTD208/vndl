//
//  ExamOverviewCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 27/04/2022.
//

import UIKit

class ExamOverviewCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: ExamOverviewVC?
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = ExamOverviewVC.factory()
            controller.coordinator = self
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
