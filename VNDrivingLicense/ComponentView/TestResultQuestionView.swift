//
//  TestResultQuestionView.swift
//  dvsa
//
//  Created by Viet Le on 04/03/2022.
//

import UIKit

class TestResultQuestionView: DimableView {
    
    func setTestResultDetailQuestion(_ testResultDetail: TestResultDetail, _ index: Int) {
        self.testResultDetail = testResultDetail
        self.indexOfQuestion = index
        self.updateUI()
    }
    
    var heightConstraint: NSLayoutConstraint!
    private var isCorrectImageView = UIImageView()
    private var questionTextLabel = UILabel()
    private var imageAnswerImageView = UIImageView()
    private var question: Question!
    private var testResultDetail: TestResultDetail!
    private var indexOfQuestion: Int!
    // MARK: - LifeCycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    private func customInit() {
        backgroundColor = UIColor.init(rgb: 0xBAC4D7).withAlphaComponent(0.2)
        cornerRadius = 20
        
        addSubview(isCorrectImageView)
        isCorrectImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            isCorrectImageView.widthAnchor.constraint(equalToConstant: 20),
            isCorrectImageView.heightAnchor.constraint(equalToConstant: 20),
            isCorrectImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            isCorrectImageView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        imageAnswerImageView.contentMode = .scaleAspectFit
        addSubview(imageAnswerImageView)
        imageAnswerImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageAnswerImageView.widthAnchor.constraint(equalToConstant: 52),
            imageAnswerImageView.heightAnchor.constraint(equalToConstant: 52),
            imageAnswerImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageAnswerImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12)
        ])
        
        questionTextLabel.numberOfLines = 0
        addSubview(questionTextLabel)
        questionTextLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            questionTextLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            questionTextLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            questionTextLabel.leadingAnchor.constraint(equalTo: isCorrectImageView.trailingAnchor, constant: 16)
        ])
        
    }
    
    private func updateUI() {
        guard let question = testResultDetail.question, let index = self.indexOfQuestion else {
            return
        }
        questionTextLabel.text = "\(index + 1). " + question.questionText
        questionTextLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        questionTextLabel.textColor = AppColor.textBlackColor
        
        if testResultDetail.isPassed() {
            isCorrectImageView.image = UIImage.init(named: "ic_pass")
        } else {
            isCorrectImageView.image = UIImage.init(named: "ic_false")
        }
        
        if question.questionImageName.isEmpty {
            imageAnswerImageView.isHidden = true
            NSLayoutConstraint.activate([
                heightAnchor.constraint(greaterThanOrEqualToConstant: 52),
                questionTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12)
            ])
        } else {
            imageAnswerImageView.isHidden = false
            imageAnswerImageView.sd_setImage(with: question.questionImageURL(), completed: nil)
            NSLayoutConstraint.activate([
                heightAnchor.constraint(greaterThanOrEqualToConstant: 84),
                questionTextLabel.trailingAnchor.constraint(equalTo: imageAnswerImageView.leadingAnchor, constant: -16)
            ])
        }
    }
}
