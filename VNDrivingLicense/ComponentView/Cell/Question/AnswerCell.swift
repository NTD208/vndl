//
//  AnswerCell.swift
//  dvsa
//
//  Created by vietlv on 19/01/2022.
//

import Foundation

protocol AnswerCell: AnyObject {
    func reload()
    func showSelectAnswer()
    func setQuestion(_ question: Question, questionIndex: Int, canAnswer: Bool)
    func showResultPractice(afterTime: TimeInterval)
    func showExamResult()
    func currentQuestion() -> Question?
}

protocol AnswerCellDelegate: AnyObject {
    func answerCell(_ cell: AnswerCell, selectAnswer answer: Int)
}
