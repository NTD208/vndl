//
//  AnswerQuestionCell.swift
//  dvsa
//
//  Created by vietlv on 18/01/2022.
//

import UIKit

class AnswerQuestionCell: UICollectionViewCell, AnswerCell {
    weak var delegate: AnswerCellDelegate?
    var question: Question?
    
    // MARK: - IBOutlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var numberQuestionLabel: UILabel!
    
    @IBOutlet weak var questionImageView: UIImageView!
    @IBOutlet weak var questionImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var questionImageViewWidthConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var answer1ContainerView: DimableView!
    @IBOutlet weak var answer1View: UIView!
    @IBOutlet weak var answer2ContainerView: DimableView!
    @IBOutlet weak var answer2View: UIView!
    @IBOutlet weak var answer3ContainerView: DimableView!
    @IBOutlet weak var answer3View: UIView!
    @IBOutlet weak var answer4ContainerView: DimableView!
    @IBOutlet weak var answer4View: UIView!
    @IBOutlet weak var answer1Label: UILabel!
    @IBOutlet weak var answer2Label: UILabel!
    @IBOutlet weak var answer3Label: UILabel!
    @IBOutlet weak var answer4Label: UILabel!
    
    @IBOutlet weak var answerViewTopConstraint: NSLayoutConstraint!
    // answer 1
    @IBOutlet weak var explain1ContainerView: UIView!
    @IBOutlet weak var explain1Label: UILabel!
    @IBOutlet weak var explainLabel1HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var answer1ContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain1ContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain1LabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain1LabelBottomConstraint: NSLayoutConstraint!
    
    // answer 2
    @IBOutlet weak var explain2ContainerView: UIView!
    @IBOutlet weak var explain2Label: UILabel!
    @IBOutlet weak var explainLabel2Constraint: NSLayoutConstraint!
    @IBOutlet weak var answer2ContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain2ContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain2LabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain2LabelBottomConstraint: NSLayoutConstraint!
    
    // answer 3
    @IBOutlet weak var answer3LabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var answer3LabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var answer3ContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var answer3ContainerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var explain3ContainerView: UIView!
    @IBOutlet weak var explain3Label: UILabel!
    @IBOutlet weak var explain3LabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain3ContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain3ContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain3LabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain3LabelBottomConstraint: NSLayoutConstraint!
    
    // answer 4
    @IBOutlet weak var answer4LabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var answer4LabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var answer4ContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var answer4ContainerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var explain4ContainerView: UIView!
    @IBOutlet weak var explain4Label: UILabel!
    @IBOutlet weak var explain4LabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain4TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var explaint4ContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain4LabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var explain4LabelBottomConstraint: NSLayoutConstraint!
    
    var canAnswer = false
    var tapDelayTime: TimeInterval = 0
    
    // MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        questionImageView.cornerRadius = 20
        
        answer1ContainerView.cornerRadius = 20
        answer1View.cornerRadius = 20
        answer2ContainerView.cornerRadius = 20
        answer2View.cornerRadius = 20
        answer3ContainerView.cornerRadius = 20
        answer3View.cornerRadius = 20
        answer4ContainerView.cornerRadius = 20
        answer4View.cornerRadius = 20
        
        explain1ContainerView.cornerRadius = 20
        explain2ContainerView.cornerRadius = 20
        explain3ContainerView.cornerRadius = 20
        explain4ContainerView.cornerRadius = 20
        reloadUI()
    }
    
    // MARK: - Public
    func setQuestion(_ question: Question, questionIndex: Int, canAnswer: Bool = true) {
        self.question = question
        self.reloadUI()

        self.questionImageView.sd_setImage(with: question.questionImageURL(), completed: {image,_,_,_ in
            if let image = image {
                if image.size.width > image.size.height {
                    let width = self.frame.width - 64
                    self.questionImageViewWidthConstraint.constant = width
                    self.questionImageViewHeightConstraint.constant = width * image.size.height / image.size.width
                } else {
                    let height: CGFloat = 200
                    self.questionImageViewHeightConstraint.constant = height
                    self.questionImageViewWidthConstraint.constant = height * image.size.width / image.size.height
                }
            }
        })

        if question.questionImageName.isEmpty {
            questionImageViewHeightConstraint.constant = 0
            answerViewTopConstraint.constant = 0
        } else {
            answerViewTopConstraint.constant = 20
        }

        self.canAnswer = canAnswer

        numberQuestionLabel.text = "Câu hỏi \(questionIndex + 1)"
        questionLabel.text = question.questionText
        answer1Label.text = question.answer_1.isEmpty ? "" : "A. " + question.answer_1
        answer2Label.text = question.answer_2.isEmpty ? "" : "B. " + question.answer_2
        answer3Label.text = question.answer_3.isEmpty ? "" : "C. " + question.answer_3
        answer4Label.text = question.answer_4.isEmpty ? "" : "D. " + question.answer_4

        self.answer4ContainerView.isHidden = question.answer_4.isEmpty
        self.answer3ContainerView.isHidden = question.answer_3.isEmpty

        self.layoutIfNeeded()
    }

    func reload() {
        reloadUI()
    }

    func showSelectAnswer() {
        reloadUI()
        let selectedAnswer = question?.selectedAnswer ?? 0
        switch selectedAnswer {
        case 1:
            answer1View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer1View.borderWidth = 0
            answer1Label.textColor = .white
        case 2:
            answer2View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer2View.borderWidth = 0
            answer2Label.textColor = .white
        case 3:
            answer3View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer3View.borderWidth = 0
            answer3Label.textColor = .white
        case 4:
            answer4View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer4View.borderWidth = 0
            answer4Label.textColor = .white
        default:
            break
        }
    }

    func showExamResult() {
        self.reloadUI()

        guard let question = question else {
            return
        }

        switch question.selectedAnswer {
        case 1:
            self.answer1View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer1View.borderWidth = 0
            self.answer1Label.textColor = .white
        case 2:
            self.answer2View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer2View.borderWidth = 0
            self.answer2Label.textColor = .white
        case 3:
            self.answer3View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer3View.borderWidth = 0
            self.answer3Label.textColor = .white
        case 4:
            self.answer4View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer4View.borderWidth = 0
            self.answer4Label.textColor = .white
        default:
            break
        }

        switch  question.answer_correct {
        case 1:
            self.answer1Label.textColor = .white
            self.answer1View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
            self.answer1View.borderWidth = 0

            self.explain1ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(500) : UILayoutPriority.init(1000)
            self.answer1ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(1000) : UILayoutPriority.init(500)
            self.explain1ContainerView.isHidden = question.explanation.isEmpty
            self.explain1Label.text = question.explanation.trim()
            let height = self.getHeighForText(self.explain1Label.text!, on: self.explain1Label)
            self.explainLabel1HeightConstraint.constant = 0
            self.scrollView.layoutIfNeeded()
            self.explainLabel1HeightConstraint.constant = question.explanation.isEmpty ? 0 : height
            self.scrollView.layoutIfNeeded()
        case 2:
            self.answer2Label.textColor = .white
            self.answer2View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
            self.answer2View.borderWidth = 0

            self.explain2ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(500) : UILayoutPriority.init(1000)
            self.answer2ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(1000) : UILayoutPriority.init(500)
            self.explain2ContainerView.isHidden = question.explanation.isEmpty
            self.explain2Label.text = question.explanation.trim()
            let height = self.getHeighForText(self.explain2Label.text!, on: self.explain2Label)
            self.explainLabel2Constraint.constant = 0
            self.scrollView.layoutIfNeeded()
            self.explainLabel2Constraint.constant = question.explanation.isEmpty ? 0 : height
            self.scrollView.layoutIfNeeded()
        case 3:
            self.answer3Label.textColor = .white
            self.answer3View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
            self.answer3View.borderWidth = 0

            self.explain3ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(500) : UILayoutPriority.init(1000)
            self.answer3ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(1000) : UILayoutPriority.init(500)
            self.explain3ContainerView.isHidden = question.explanation.isEmpty
            self.explain3Label.text = question.explanation.trim()
            let height = self.getHeighForText(self.explain3Label.text!, on: self.explain3Label)
            self.explain3LabelHeightConstraint.constant = 0
            self.scrollView.layoutIfNeeded()
            self.explain3LabelHeightConstraint.constant = question.explanation.isEmpty ? 0 : height
            self.scrollView.layoutIfNeeded()
        case 4:
            self.answer4Label.textColor = .white
            self.answer4View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
            self.answer4View.borderWidth = 0

            self.explaint4ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(500) : UILayoutPriority.init(1000)
            self.answer4ContainerBottomConstraint.priority = question.explanation.isEmpty ? UILayoutPriority.init(1000) : UILayoutPriority.init(500)
            self.explain4ContainerView.isHidden = question.explanation.isEmpty
            self.explain4Label.text = question.explanation.trim()
            let height = self.getHeighForText(self.explain4Label.text!, on: self.explain4Label)
            self.explain4LabelHeightConstraint.constant = 0
            self.scrollView.layoutIfNeeded()
            self.explain4LabelHeightConstraint.constant = question.explanation.isEmpty ? 0 : height
            self.scrollView.layoutIfNeeded()
        default:
            break
        }
    }

    func showResultPractice(afterTime: TimeInterval = 0) {
        self.reloadUI()

        guard let question = question else {
            return
        }

        switch question.practice_answer {
        case 1:
            self.answer1ContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            self.answer1View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer1View.borderWidth = 0
        case 2:
            self.answer2ContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            self.answer2View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer2View.borderWidth = 0
        case 3:
            self.answer3ContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            self.answer3View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer3View.borderWidth = 0
        case 4:
            self.answer4ContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            self.answer4View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer4View.borderWidth = 0
        default:
            break
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + afterTime) {
            switch question.practice_answer {
            case 1:
                self.answer1Label.textColor = .white
                self.answer1ContainerView.backgroundColor = UIColor.init(rgb: 0xDA4343)
                self.answer1View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
                self.answer1View.borderWidth = 0
            case 2:
                self.answer2Label.textColor = .white
                self.answer2ContainerView.backgroundColor = UIColor.init(rgb: 0xDA4343)
                self.answer2View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
                self.answer2View.borderWidth = 0
            case 3:
                self.answer3Label.textColor = .white
                self.answer3ContainerView.backgroundColor = UIColor.init(rgb: 0xDA4343)
                self.answer3View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
                self.answer3View.borderWidth = 0
            case 4:
                self.answer4Label.textColor = .white
                self.answer4ContainerView.backgroundColor = UIColor.init(rgb: 0xDA4343)
                self.answer4View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
                self.answer4View.borderWidth = 0
            default:
                break
            }

            switch  question.answer_correct {
            case 1:
                self.answer1Label.textColor = .white
                self.answer1ContainerView.backgroundColor = UIColor.init(rgb: 0x42B5A4)
                self.answer1View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
                self.answer1View.borderWidth = 0

                self.explain1ContainerBottomConstraint.priority = UILayoutPriority.init(1000)
                self.answer1ContainerBottomConstraint.priority = UILayoutPriority.init(500)
                self.explain1ContainerView.isHidden = false
                self.explain1Label.text = question.explanation.trim()
                let height = self.getHeighForText(self.explain1Label.text!, on: self.explain1Label)
                self.explainLabel1HeightConstraint.constant = 0
                self.explain1LabelTopConstraint.constant = 0
                self.explain1LabelBottomConstraint.constant = 0
                self.scrollView.layoutIfNeeded()
                UIView.animate(withDuration: afterTime == 0 ? 0 : 0.3) {
                    self.explainLabel1HeightConstraint.constant = question.explanation.isEmpty ? 0 : height
                    self.explain1LabelTopConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.explain1LabelBottomConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.scrollView.layoutIfNeeded()
                }
            case 2:
                self.answer2Label.textColor = .white
                self.answer2ContainerView.backgroundColor = UIColor.init(rgb: 0x42B5A4)
                self.answer2View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
                self.answer2View.borderWidth = 0

                self.explain2ContainerBottomConstraint.priority = UILayoutPriority.init(1000)
                self.answer2ContainerBottomConstraint.priority = UILayoutPriority.init(500)
                self.explain2ContainerView.isHidden = false
                self.explain2Label.text = question.explanation.trim()
                let height = self.getHeighForText(self.explain2Label.text!, on: self.explain2Label)
                self.explainLabel2Constraint.constant = 0
                self.explain2LabelTopConstraint.constant = 0
                self.explain2LabelBottomConstraint.constant = 0
                self.scrollView.layoutIfNeeded()
                UIView.animate(withDuration: afterTime == 0 ? 0 : 0.3) {
                    self.explainLabel2Constraint.constant = question.explanation.isEmpty ? 0 : height
                    self.explain2LabelTopConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.explain2LabelBottomConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.scrollView.layoutIfNeeded()
                }
            case 3:
                self.answer3Label.textColor = .white
                self.answer3ContainerView.backgroundColor = UIColor.init(rgb: 0x42B5A4)
                self.answer3View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
                self.answer3View.borderWidth = 0

                self.explain3ContainerBottomConstraint.priority = UILayoutPriority.init(1000)
                self.answer3ContainerBottomConstraint.priority = UILayoutPriority.init(500)
                self.explain3ContainerView.isHidden = false
                self.explain3Label.text = question.explanation.trim()
                let height = self.getHeighForText(self.explain3Label.text!, on: self.explain3Label)
                self.explain3LabelHeightConstraint.constant = 0
                self.explain3LabelTopConstraint.constant = 0
                self.explain3LabelBottomConstraint.constant = 0
                self.scrollView.layoutIfNeeded()
                UIView.animate(withDuration: afterTime == 0 ? 0 : 0.3) {
                    self.explain3LabelHeightConstraint.constant = question.explanation.isEmpty ? 0 : height
                    self.explain3LabelTopConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.explain3LabelBottomConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.scrollView.layoutIfNeeded()
                }
            case 4:
                self.answer4Label.textColor = .white
                self.answer4ContainerView.backgroundColor = UIColor.init(rgb: 0x42B5A4)
                self.answer4View.backgroundColor = UIColor.init(rgb: 0x0CC79A)
                self.answer4View.borderWidth = 0

                self.explaint4ContainerBottomConstraint.priority = UILayoutPriority.init(1000)
                self.answer4ContainerBottomConstraint.priority = UILayoutPriority.init(500)
                self.explain4ContainerView.isHidden = false
                self.explain4Label.text = question.explanation.trim()
                let height = self.getHeighForText(self.explain4Label.text!, on: self.explain4Label)
                self.explain4LabelHeightConstraint.constant = 0
                self.explain4LabelTopConstraint.constant = 0
                self.explain4LabelBottomConstraint.constant = 0
                self.scrollView.layoutIfNeeded()
                UIView.animate(withDuration: afterTime == 0 ? 0 : 0.3) {
                    self.explain4LabelHeightConstraint.constant = question.explanation.isEmpty ? 0 : height
                    self.explain4LabelTopConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.explain4LabelBottomConstraint.constant = question.explanation.isEmpty ? 0 : 15
                    self.scrollView.layoutIfNeeded()
                }
            default:
                break
            }

            self.scrollView.layoutIfNeeded()
        }
    }

    func currentQuestion() -> Question? {
        return self.question
    }

    // MARK: - IBAction

    @IBAction func answer1ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 1)
    }

    @IBAction func answer2ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 2)
    }

    @IBAction func answer3ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 3)
    }

    @IBAction func answer4ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 4)
    }

    // MARK: - Helper
    private func reloadUI() {
        answer1View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer1View.borderWidth = 1
        answer1View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer1Label.textColor = UIColor.init(rgb: 0x212121)

        answer2View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer2View.borderWidth = 1
        answer2View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer2Label.textColor = UIColor.init(rgb: 0x212121)

        answer3View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer3View.borderWidth = 1
        answer3View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer3Label.textColor = UIColor.init(rgb: 0x212121)

        answer4View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer4View.borderWidth = 1
        answer4View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer4Label.textColor = UIColor.init(rgb: 0x212121)

        reloadLayout()
    }

    private func reloadLayout() {
        guard let question = question else {
            return
        }

        explain1ContainerView.isHidden = true
        explain1ContainerBottomConstraint.priority = UILayoutPriority.init(500)
        answer1ContainerView.isHidden = false
        answer1ContainerBottomConstraint.priority = UILayoutPriority.init(1000)

        explain2ContainerView.isHidden = true
        explain2ContainerBottomConstraint.priority = UILayoutPriority.init(500)
        answer2ContainerView.isHidden = false
        answer2ContainerBottomConstraint.priority = UILayoutPriority.init(1000)

        explain3ContainerBottomConstraint.priority = UILayoutPriority.init(500)
        explain3ContainerView.isHidden = true
        answer3ContainerHeightConstraint.constant = 52
        answer3ContainerBottomConstraint.priority = UILayoutPriority.init(1000)
        answer3ContainerView.isHidden = false
        answer3LabelTopConstraint.constant = 8
        answer3LabelBottomConstraint.constant = 8

        explaint4ContainerBottomConstraint.priority = UILayoutPriority.init(500)
        explain4ContainerView.isHidden = true
        answer4ContainerHeightConstraint.constant = 52
        answer4ContainerBottomConstraint.priority = UILayoutPriority.init(1000)
        answer4ContainerView.isHidden = false
        answer4LabelTopConstraint.constant = 8
        answer4LabelBottomConstraint.constant = 8

        answer4ContainerHeightConstraint.constant = question.answer_4.isEmpty ? 0 : 52
        answer4ContainerBottomConstraint.constant = question.answer_4.isEmpty ? 0 : 15
        answer4LabelTopConstraint.constant = question.answer_4.isEmpty ? 0 : 8
        answer4LabelBottomConstraint.constant = question.answer_4.isEmpty ? 0 : 8

        answer3ContainerHeightConstraint.constant = question.answer_3.isEmpty ? 0 : 52
        answer3ContainerBottomConstraint.constant = question.answer_3.isEmpty ? 0 : 15
        answer3LabelTopConstraint.constant = question.answer_3.isEmpty ? 0 : 8
        answer3LabelBottomConstraint.constant = question.answer_3.isEmpty ? 0 : 8

        answer3ContainerView.isHidden = question.answer_3.isEmpty
        answer3View.isHidden = question.answer_3.isEmpty

        answer4ContainerView.isHidden = question.answer_4.isEmpty
        answer4View.isHidden = question.answer_4.isEmpty
        layoutIfNeeded()
    }

    private func getHeighForText(_ text: String, on label: UILabel) -> CGFloat {
        let testLabel = UILabel.init()
        testLabel.frame = CGRect.init(x: 0, y: 0, width: label.frame.width, height: 1000)
        testLabel.numberOfLines = 0
        testLabel.text = text
        testLabel.font = label.font
        testLabel.sizeToFit()
        return testLabel.frame.height
    }
}
