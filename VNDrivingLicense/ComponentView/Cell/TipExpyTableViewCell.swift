//
//  TipExpyTableViewCell.swift
//  dmv
//
//  Created by Khue Minh on 11/06/2021.
//

import UIKit
import ExpyTableView

class TipExpyTableViewCell: UITableViewCell, ExpyTableViewHeaderCell {
    @IBOutlet weak var tipTitleLabel: UILabel!
    @IBOutlet weak var orienttationImageView: UIImageView!
    
    @IBOutlet weak var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(_ tip: Tip) {
        self.numberLabel.text = "\(tip.type)"
        self.tipTitleLabel.text = tip.name
    }
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            hideSeparator()
            self.orienttationImageView.image = UIImage.init(named: "ic_arrow_up")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            self.orienttationImageView.image = UIImage.init(named: "ic_arrow_down")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            showSeparator()
            print("DID COLLAPSE")
        }
    }
}
