//
//  TipTableViewCell.swift
//  dmv
//
//  Created by Khue Minh on 11/06/2021.
//

import UIKit

class TipTableViewCell: UITableViewCell {

    @IBOutlet weak var tipNoteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(_ tip: Tip, index: Int) {
        self.tipNoteLabel.text = tip.tips[index - 1]
    }
    
}
