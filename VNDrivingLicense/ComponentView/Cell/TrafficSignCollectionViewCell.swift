//
//  TrafficSignCollectionViewCell.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit
import SDWebImage

class TrafficSignCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bindData(_ sign: Sign) {
        self.imageView.sd_setImage(with: sign.imageURL())
        self.titleLabel.text = sign.signCode
        self.infoLabel.text = sign.title
    }

}
