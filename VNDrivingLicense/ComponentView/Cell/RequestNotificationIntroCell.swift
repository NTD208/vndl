//
//  RequestNotificationIntroCell.swift
//  VNDrivingLicense
//
//  Created by DuNT on 02/04/2022.
//

import UIKit

protocol RequestNotifyIntroCellDelegate: AnyObject {
    func requestNotifyIntroCellDidTapContinueButton(_ cell: RequestNotificationIntroCell)
}

class RequestNotificationIntroCell: UICollectionViewCell {
    weak var delegate: RequestNotifyIntroCellDelegate?

    // MARK: - IBOutlet
    @IBOutlet weak var continueButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let attributedString = NSMutableAttributedString(string: "TIẾP TỤC")
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 20, weight: .bold), range: NSRange(location: 0, length: attributedString.length))
        continueButton.setAttributedTitle(attributedString, for: .normal)
    }

    @IBAction func continueButtonDidTap(_ sender: Any) {
        Permission.requestNotificationPermission { isSuccess in
            self.delegate?.requestNotifyIntroCellDidTapContinueButton(self)
        }
    }
}
