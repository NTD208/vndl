//
//  ChooseTestDateIntroCell.swift
//  VNDrivingLicense
//
//  Created by DuNT on 02/04/2022.
//

import UIKit

protocol ChooseTestDateIntroCellDelegate: AnyObject {
    func chooseTestDateIntroCell(_ cell: ChooseTestDateIntroCell, didTapContinue testDate: Date?)
}

class ChooseTestDateIntroCell: UICollectionViewCell {
    weak var delegate: ChooseTestDateIntroCellDelegate?
    weak var owner: UIViewController?
    
    // MARK: - IBOutlet
    @IBOutlet weak var testDateButton: DimableView!
    @IBOutlet weak var testDateLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    var date: Date? {
        didSet {
            if date == nil {
                testDateLabel.textColor = UIColor(rgb: 0xBABABA)
                testDateLabel.text = "DD/MM/YYYY"
            } else {
                testDateLabel.textColor = AppColor.textBlackColor
                testDateLabel.text = Utils.stringFromDate(date!, format: "dd/MM/yyyy")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let attributedString = NSMutableAttributedString(string: "TIẾP TỤC")
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 20, weight: .bold), range: NSRange(location: 0, length: attributedString.length))
        continueButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    func bind(date: Date?, owner: UIViewController?) {
        self.date = date
        self.owner = owner
    }

    @IBAction func testDateButtonDidTap(_ sender: Any) {
        let calendarController = CalendarViewController.init()
        calendarController.delegate = self
        calendarController.selectedDate = AppState.testDate
        guard let owner = self.owner else {
            return
        }
        calendarController.show(on: owner)
    }
    
    @IBAction func continueButtonDidTap(_ sender: Any) {
        delegate?.chooseTestDateIntroCell(self, didTapContinue: self.date)
    }
    
}

extension ChooseTestDateIntroCell: CalendarViewControllerDelegate {
    func calendarViewController(_ controller: CalendarViewController, didSelectDate date: Date) {
        self.date = date
        AppState.totalDate = date.dayNumber(from: Date())
        AppState.testDate = date
    }
}
