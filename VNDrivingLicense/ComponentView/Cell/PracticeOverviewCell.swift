//
//  PracticeOverviewCell.swift
//  VNDrivingLicense
//
//  Created by DuNT on 24/04/2022.
//

import UIKit

class PracticeOverviewCell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var progressView: ProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindData(_ questionType: QuestionType) {
        myImageView.image = UIImage(named: questionType.imageName)
        titleLabel.text = questionType.title
        descLabel.text = questionType.desc
        
        progressView.bgColor = UIColor(rgb: 0xEAEDF3)
        progressView.color = UIColor(rgb: 0xF2994A)
        progressLabel.text = "\(questionType.correctCount + questionType.incorrectCount)/\(questionType.totalQuestion)"
        progressView.progress = CGFloat(questionType.correctCount + questionType.incorrectCount) / CGFloat(questionType.totalQuestion)
    }
    
}
