//
//  ExamOverviewCell.swift
//  VNDrivingLicense
//
//  Created by DuNT on 27/04/2022.
//

import UIKit

class ExamOverviewCell: UICollectionViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var testInfoV1: UIView!
    @IBOutlet weak var titleLabelV1: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var progressCircleView: ProgressCircleView!
    @IBOutlet weak var progressLabel: UILabel!
    
    @IBOutlet weak var testInfoV2: UIView!
    @IBOutlet weak var titleLabelV2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        shadowColor = .black
        shadowRadius = 12
        shadowOpacity = 0.04
        shadowOffset = CGSize(width: 0, height: 0)
    }
    
    func bindData(_ test: Test) {
        
        if let testResult = test.testResult {
            testInfoV1.isHidden = false
            testInfoV2.isHidden = true
            
            titleLabelV1.text = test.testName
            let date = Utils.dateFromString(testResult.testTime, format: DateFormat.longFormat)
            timeLabel.text = Utils.stringFromDate(date!, format: "dd/MM/yyyy")
            
            let isPassed = testResult.isPassed()
            let headText = NSMutableAttributedString(string: "\(testResult.numberAnswerCorrect ?? 0)")
            headText.addAttribute(.font, value: UIFont.systemFont(ofSize: 22, weight: .medium), range: NSRange(location: 0, length: headText.length))
            let tailText = NSMutableAttributedString(string: "/\(testResult.totalQuestion ?? 0)")
            tailText.addAttribute(.font, value: UIFont.systemFont(ofSize: 16, weight: .regular), range: NSRange(location: 0, length: tailText.length))
            headText.append(tailText)
            progressLabel.attributedText = headText
            progressLabel.textColor = isPassed ? AppColor.correctColor : AppColor.wrongColor
            
            progressCircleView.lineWidth = 8
            progressCircleView.correctColor = AppColor.correctColor
            progressCircleView.wrongColor = AppColor.wrongColor
            progressCircleView.correctProgress = CGFloat(testResult.numberAnswerCorrect) / CGFloat(testResult.totalQuestion)
            progressCircleView.wrongProgress = 1.0
            
        } else {
            testInfoV1.isHidden = true
            testInfoV2.isHidden = false
            
            titleLabelV2.text = test.testName
        }
    }

}
