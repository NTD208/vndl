//
//  HeadMenuCollectionViewCell.swift
//  CustomViewExp
//
//  Created by Chu Du on 10/11/2021.
//

import UIKit

class HeadMenuCollectionViewCell: UICollectionViewCell {
    // MARK: - IBOutlet
    @IBOutlet weak var ItemLabel: UILabel!
    @IBOutlet weak var contenView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setSelected(_ selected: Bool) {
        if selected {
            ItemLabel.textColor = UIColor(rgb: 0xFFFFFF)
            contenView.backgroundColor = UIColor(rgb: 0x4877D8)
        } else {
            ItemLabel.textColor = UIColor(rgb: 0x212121)
            contenView.backgroundColor = UIColor(rgb: 0xFFFFFF)
        }
    }
    
    func config(item:String, isSelected: Bool) {
        ItemLabel.text = item
        if isSelected {
            ItemLabel.textColor = UIColor(rgb: 0xFFFFFF)
            contenView.backgroundColor = UIColor(rgb: 0x4877D8)
        } else {
            ItemLabel.textColor = UIColor(rgb: 0x212121)
            self.contenView.backgroundColor = UIColor(rgb: 0xFFFFFF)
        }
    }

}
