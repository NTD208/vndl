//
//  HeadMenu.swift
//  CustomViewExp
//
//  Created by Thom Vu on 05/11/2021.
//

import Foundation
import UIKit

protocol HeadMenuDataSource: AnyObject {
    func headMenuListItem(_ headMenu: HeadMenu) -> [String]
}

protocol HeadMenuDelegate: AnyObject {
    func headMenu(_ headMenu: HeadMenu, didSelectItemAtIndex index: Int)
}

class HeadMenu: UIView {
    weak var delegate: HeadMenuDelegate?
    weak var dataSource: HeadMenuDataSource? {
        didSet {
            reloadData()
        }
    }
    
    func reloadData() {
        self.items = dataSource?.headMenuListItem(self) ?? [String]()
        self.selectedIndex = 0
        collectionView.reloadData()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    var collectionView: UICollectionView!
    
    var items = [String]()
    var selectedIndex = 0
    
    private func customInit() {
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        flowlayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        
        NSLayoutConstraint.activate([
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: topAnchor)
        ])
        
        collectionView.registerCell(type: HeadMenuCollectionViewCell.self)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension HeadMenu: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: HeadMenuCollectionViewCell.self, indexPath: indexPath) else { return UICollectionViewCell() }
        cell.config(item: items[indexPath.row], isSelected: selectedIndex == indexPath.row)
        return cell
    }
}

extension HeadMenu: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        return CGSize(width: 110, height: 42)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let oldIndexPath = IndexPath.init(row: selectedIndex, section: 0)
        
        if let oldCell = collectionView.cellForItem(at: oldIndexPath) as? HeadMenuCollectionViewCell {
            oldCell.setSelected(false)
        }
        
        if let cell = collectionView.cellForItem(at: indexPath) as? HeadMenuCollectionViewCell {
            cell.setSelected(true)
        }

        selectedIndex = indexPath.row
        self.delegate?.headMenu(self, didSelectItemAtIndex: selectedIndex)
        collectionView.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
}
