//
//  DIContainer.swift
//  dmv
//
//  Created by Thom Vu on 25/05/2021.
//

import Foundation
import Swinject

class DIContainer {
    static let shared = DIContainer()
    
    var container: Container!
    init() {
        self.container = Container()
        
        //Intro
        registerStartIntroViewModel()
        registerIntroViewModel()
        
        //Splash
        registerSplashViewModel()
        
        //Home
        registerHomeViewModel()
        
        //TrafficSign
        registerTrafficSignViewModel()
        registerTrafficSignDetailViewModel()
        
        //Tip
        registerTipViewModel()
        
        //Practice
        registerPracticeOverviewViewModel()
        registerPracticeViewModel()
        
        //Exam
        registerExamOverviewViewModel()
        registerExamViewModel()
        registerExamReviewViewModel()
        registerExamResultDetailViewModel()
        
        //Setting
        registerSettingViewModel()
    }
    
    // MARK: - Intro
    func registerStartIntroViewModel() {
        self.container.register(StartIntroViewModelFactory.self) { _ in
            return StartIntroViewModel()
        }
    }
    
    func registerIntroViewModel() {
        self.container.register(IntroViewModelFactory.self) { _ in
            return IntroViewModel()
        }
    }
    
    // MARK: - Splash
    func registerSplashViewModel() {
        self.container.register(SplashViewModelFactory.self) { _ in
            return SplashViewModel()
        }
    }
    
    // MARK: - Home
    func registerHomeViewModel() {
        self.container.register(HomeViewModelFactory.self) { _ in
            return HomeViewModel()
        }
    }
    
    // MARK: - TrafficSign
    func registerTrafficSignViewModel() {
        self.container.register(TrafficSignViewModelFactory.self) { _ in
            return TrafficSignViewModel()
        }
    }
    
    func registerTrafficSignDetailViewModel() {
        self.container.register(TrafficSignDetailViewModelFactory.self) { _ in
            return TrafficSignDetailViewModel()
        }
    }
    
    // MARK: - Tip
    func registerTipViewModel() {
        self.container.register(TipViewModelFactory.self) { _ in
            return TipViewModel()
        }
    }
    
    // MARK: - Practice
    func registerPracticeOverviewViewModel() {
        self.container.register(PracticeOverviewViewModelFactory.self) { _ in
            return PracticeOverviewViewModel()
        }
    }
    
    func registerPracticeViewModel() {
        self.container.register(PracticeViewModelFactory.self) { _ in
            return PracticeViewModel()
        }
    }
    
    // MARK: - Exam
    func registerExamOverviewViewModel() {
        self.container.register(ExamOverviewViewModelFactory.self) { _ in
            return ExamOverviewViewModel()
        }
    }
    
    func registerExamViewModel() {
        self.container.register(ExamViewModelFactory.self) { _ in
            return ExamViewModel()
        }
    }
    
    func registerExamReviewViewModel() {
        self.container.register(ExamReviewViewModelFactory.self) { _ in
            return ExamReviewViewModel()
        }
    }
    
    func registerExamResultDetailViewModel() {
        self.container.register(ExamResultDetailViewModelFactory.self) { _ in
            return ExamResultDetailViewModel()
        }
    }
    
    // MARK: - Setting
    func registerSettingViewModel() {
        self.container.register(SettingViewModelFactory.self) { _ in
            return SettingViewModel()
        }
    }
}
